<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\View;

/**
 * Layout model
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.ExcessiveParameterList)
 */
class Layout extends \MegaCodex\Framework\Simplexml\Config
{
    /**
     * @var bool
     */
    protected $isBuilt;

    /**
     * @var \MegaCodex\Framework\View\Layout\Processor
     */
    protected $update;

    /**
     * @var \MegaCodex\Framework\View\Theme\Resolver
     */
    protected $themeResolver;

    /**
     * @var \MegaCodex\Framework\View\Layout\ProcessorFactory
     */
    protected $processorFactory;

    /**
     * @var \MegaCodex\Framework\View\Layout\XmlStructure
     */
    protected $structure;

    /**
     * @var \MegaCodex\Framework\App\Cache
     */
    protected $cache;

    /**
     * @var Layout\Reader\ContextFactory
     */
    protected $readerContextFactory;

    /**
     * @var Layout\Generator\ContextFactory
     */
    protected $generatorContextFactory;

    /**
     * @var Layout\Reader\Context
     */
    protected $readerContext;

    /**
     * @var \MegaCodex\Framework\View\Layout\ReaderPool
     */
    protected $readerPool;

    /**
     * @var \MegaCodex\Framework\View\Layout\GeneratorPool
     */
    protected $generatorPool;

    public function __construct(
        \MegaCodex\Framework\View\Theme\Resolver $themeResolver,
        \MegaCodex\Framework\View\Layout\ProcessorFactory $processorFactory,
        \MegaCodex\Framework\View\Layout\XmlStructure $structure,
        \MegaCodex\Framework\View\Layout\Reader\ContextFactory $readerContextFactory,
        \MegaCodex\Framework\View\Layout\Generator\ContextFactory $generatorContextFactory,
        \MegaCodex\Framework\View\Layout\ReaderPool $readerPool,
        \MegaCodex\Framework\View\Layout\GeneratorPool $generatorPool,
        \MegaCodex\Framework\App\Cache $cache
    ) {
        $this->themeResolver = $themeResolver;
        $this->processorFactory = $processorFactory;
        $this->structure = $structure;
        $this->cache = $cache;
        $this->readerContextFactory = $readerContextFactory;
        $this->generatorContextFactory = $generatorContextFactory;
        $this->readerPool = $readerPool;
        $this->generatorPool = $generatorPool;
        $this->isBuilt = false;
        $this->update = false;
    }

    public function getOutput()
    {
        $this->build();
        $out = "";
        foreach ($this->_output as $name) {
            $out .= $this->renderElement($name);
        }
        return $out;
    }

    protected function build()
    {
        if (!$this->isBuilt) {
            $this->isBuilt = true;
            $this->getUpdate();
            $this->generateXml();
            $this->generateBlocks();
        }
    }

    /**
     * Retrieve the layout update instance
     *
     * @return \Magento\Framework\View\Layout\ProcessorInterface
     */
    public function getUpdate()
    {
        if (!$this->update) {
            $theme = $this->themeResolver->get();
            $this->update = $this->processorFactory->create(["theme" => $theme]);
        }
        return $this->update;
    }

    /**
     * Layout xml generation
     *
     * @return $this
     */
    public function generateXml()
    {
        $xml = $this->getUpdate()->asSimplexml();
        $this->setXml($xml);
        $this->structure->importElements([]);
        return $this;
    }

    /**
     * Create structure of elements from the loaded XML configuration
     *
     * @return void
     */
    public function generateBlocks()
    {
        $cacheId = "structure_" . $this->getUpdate()->getCacheId();
        $result = $this->cache->load($cacheId);
        if ($result) {
            $data = \Zend\Json\Json::decode($result, \Zend\Json\Json::TYPE_ARRAY);
            $this->getReaderContext()->getPageConfigStructure()->populateWithArray($data["pageConfigStructure"]);
            $this->getReaderContext()->getScheduledStructure()->populateWithArray($data["scheduledStructure"]);
        } else {
            $this->readerPool->interpret($this->getReaderContext(), $this->getNode());
            $data = [
                "pageConfigStructure" => $this->getReaderContext()->getPageConfigStructure()->__toArray(),
                "scheduledStructure"  => $this->getReaderContext()->getScheduledStructure()->__toArray(),
            ];
            var_dump($data);
            die();
            $this->cache->save(\Zend\Json\Json::encode($data), $cacheId, $this->getUpdate()->getHandles());
        }

        $generatorContext = $this->generatorContextFactory->create(
            [
                "structure" => $this->structure,
                "layout"    => $this,
            ]
        );

        $this->generatorPool->process($this->getReaderContext(), $generatorContext);

        $this->addToOutputRootContainers();
        \Magento\Framework\Profiler::stop(__CLASS__ . "::" . __METHOD__);
    }

    public function getReaderContext()
    {
        if (!$this->readerContext) {
            $this->readerContext = $this->readerContextFactory->create();
        }
        return $this->readerContext;
    }
}
