<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\View;

interface AssetInterface
{
    public function getPath();

    public function publish();

    public function setPaths($filePath, $module, $theme);
}
