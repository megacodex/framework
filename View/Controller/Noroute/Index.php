<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\Controller\Noroute;

class Index extends \MegaCodex\Framework\App\Action\AbstractAction
{
    protected function execute()
    {
        $this->getResponse()->setStatusHeader(404, "1.1", "Not Found");
        $this->getResponse()->setHeader("Status", "404 Page not found");
        $this->getResponse()->setBody("404 Page not found");
    }
}
