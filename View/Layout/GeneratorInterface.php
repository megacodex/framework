<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\View\Layout;

/**
 * Interface \MegaCodex\Framework\View\Layout\GeneratorInterface
 */
interface GeneratorInterface
{

    /**
     * @param \MegaCodex\Framework\View\Layout\MegaCodex\Framework\View\Layout\Reader\Context $readerContext
     * @param \MegaCodex\Framework\View\Layout\MegaCodex\Framework\View\Layout\Generator\Context $generatorContext
     * * @return $this
     */
    public function process(
        MegaCodex\Framework\View\Layout\Reader\Context $readerContext,
        MegaCodex\Framework\View\Layout\Generator\Context $generatorContext
    );

    /**
     * Return type of generator
     *
     * @return string
     */
    public function getType();
}
