<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\View\Layout;

/**
 * Factory class for Processor
 */
class ProcessorFactory
{
    /**
     * @var \MegaCodex\Framework\App\ObjectManager
     */
    protected $_objectManager;

    /**
     * @param \MegaCodex\Framework\App\ObjectManager $objectManager
     */
    public function __construct(\MegaCodex\Framework\App\ObjectManager $objectManager)
    {
        $this->_objectManager = $objectManager;
    }

    /**
     * Create class instance with specified parameters
     *
     * @param array $data
     *
     * @return ProcessorInterface
     */
    public function create(array $data = [])
    {
        return $this->_objectManager->create(\MegaCodex\Framework\View\Layout\Processor::class, $data);
    }
}
