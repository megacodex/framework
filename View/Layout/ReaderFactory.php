<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\View\Layout;

/**
 * Class ReaderFactory
 */
class ReaderFactory
{
    /**
     * @var \MegaCodex\Framework\App\ObjectManager
     */
    protected $objectManager;

    /**
     * @param \MegaCodex\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(\MegaCodex\Framework\App\ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * Create reader instance with specified parameters
     *
     * @param string $className
     * @param array $data
     *
     * @return \MegaCodex\Framework\View\Layout\ReaderInterface
     * @throws \InvalidArgumentException
     */
    public function create($className, array $data = [])
    {
        $reader = $this->objectManager->create($className, $data);
        if (!$reader instanceof \MegaCodex\Framework\View\Layout\ReaderInterface) {
            throw new \InvalidArgumentException(
                $className . " doesn't implement \MegaCodex\Framework\View\Layout\ReaderInterface"
            );
        }
        return $reader;
    }
}
