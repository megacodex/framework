<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\View\Layout;

/**
 * Class Pool
 */
class ReaderPool implements \MegaCodex\Framework\View\Layout\ReaderInterface
{
    /**
     * @var array
     */
    protected $readers;

    /**
     * @var Layout\ReaderInterface[]
     */
    protected $nodeReaders = [];

    /**
     * Object Manager
     *
     * @var \Magento\Framework\View\Layout\ReaderFactory
     */
    protected $readerFactory;

    /**
     * Constructor
     *
     * @param Layout\ReaderFactory $readerFactory
     * @param array $readers
     */
    public function __construct(
        \MegaCodex\Framework\View\Layout\ReaderFactory $readerFactory,
        array $readers = null
    ) {
        $this->readerFactory = $readerFactory;
        $this->readers = $readers;
    }

    /**
     * Add reader to the pool
     *
     * @param Layout\ReaderInterface $reader
     *
     * @return $this
     */
    public function addReader(\MegaCodex\Framework\View\Layout\RenderInterface $reader)
    {
        foreach ($reader->getSupportedNodes() as $nodeName) {
            $this->nodeReaders[$nodeName] = $reader;
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     * @return string[]
     */
    public function getSupportedNodes()
    {
        return array_keys($this->nodeReaders);
    }

    /**
     * Register supported nodes and readers
     *
     * @param array $readers
     *
     * @return void
     */
    protected function prepareReader($readers)
    {
        if (empty($this->nodeReaders)) {
            /** @var $reader Layout\ReaderInterface */
            foreach ($readers as $readerClass) {
                $reader = $this->readerFactory->create($readerClass);
                $this->addReader($reader);
            }
        }
    }

    /**
     * Traverse through all nodes
     *
     * @param Reader\Context $readerContext
     * @param Layout\Element $element
     *
     * @return $this
     */
    public function interpret(
        \MegaCodex\Framework\View\Layout\Reader\Context $readerContext,
        \MegaCodex\Framework\View\Layout\Element $element
    ) {
        $this->prepareReader($this->readers);
        /** @var $node Layout\Element */
        foreach ($element as $node) {
            $nodeName = $node->getName();
            if (!isset($this->nodeReaders[$nodeName])) {
                continue;
            }
            /** @var $reader Layout\ReaderInterface */
            $reader = $this->nodeReaders[$nodeName];
            $reader->interpret($readerContext, $node, $element);
        }
        return $this;
    }
}
