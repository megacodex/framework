<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\View\Layout\Condition;

/**
 * Factory for composite.
 */
class ConditionFactory
{
    /**
     * @var \MegaCodex\Framework\App\ObjectManager
     */
    protected $objectManager;

    /**
     * @param \MegaCodex\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(\MegaCodex\Framework\App\ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param array $elementVisibilityConditions
     *
     * @return Condition
     */
    public function create(array $elementVisibilityConditions)
    {
        $conditions = [];
        foreach ($elementVisibilityConditions as $condition) {
            $conditions[] = $this->objectManager->create($condition['name']);
        }
        return $this->objectManager->create(Condition::class, ['conditions' => $conditions]);
    }
}
