<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\View\Layout\Generator;

/**
 * @api
 */
class Context
{
    /**
     * @var Layout\Data\Structure
     */
    protected $structure;

    /**
     * @var LayoutInterface
     */
    protected $layout;

    /**
     * Constructor
     *
     * @param Layout\Data\Structure $structure
     * @param LayoutInterface $layout
     */
    public function __construct(
        \MegaCodex\Framework\View\Layout\XmlStructure $structure,
        \MegaCodex\Framework\View\Layout $layout
    ) {
        $this->structure = $structure;
        $this->layout = $layout;
    }

    /**
     * @return \Magento\Framework\View\Layout\Data\Structure
     */
    public function getStructure()
    {
        return $this->structure;
    }

    /**
     * @return LayoutInterface
     */
    public function getLayout()
    {
        return $this->layout;
    }
}
