<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\View\Layout\Reader;

class ContextFactory
{
    /**
     * @var \MegaCodex\Framework\App\ObjectManager
     */
    protected $objectManager;

    /**
     * @param \MegaCodex\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(\MegaCodex\Framework\App\ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * Create class instance with specified parameters
     *
     * @param array $data
     *
     * @return \MegaCodex\Framework\View\Layout\Reader\Context
     */
    public function create(array $data = [])
    {
        return $this->objectManager->create(\MegaCodex\Framework\View\Layout\Reader\Context::class, $data);
    }
}
