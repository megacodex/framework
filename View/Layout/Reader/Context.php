<?php
/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\View\Layout\Reader;

/**
 * @api
 */
class Context
{

    /**
     * @var \MegaCodex\Framework\View\Page\Config\Structure
     */
    protected $pageConfigStructure;

    /**
     * Constructor
     *
     * @param \MegaCodex\Framework\View\Layout\ScheduledStructure $scheduledStructure
     * @param \MegaCodex\Framework\View\Page\Config\Structure $pageConfigStructure
     */
    public function __construct(
        \MegaCodex\Framework\View\Layout\ScheduledStructure $scheduledStructure,
        \MegaCodex\Framework\View\Page\Config\Structure $pageConfigStructure
    ) {
        $this->scheduledStructure = $scheduledStructure;
        $this->pageConfigStructure = $pageConfigStructure;
    }

    /**
     * @return \MegaCodex\Framework\View\Page\Config\Structure
     */
    public function getPageConfigStructure()
    {
        return $this->pageConfigStructure;
    }

    /**
     * @return \MegaCodex\Framework\View\Layout\ScheduledStructure
     */
    public function getScheduledStructure()
    {
        return $this->scheduledStructure;
    }
}
