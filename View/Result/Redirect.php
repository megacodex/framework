<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\View\Result;

/**
 * In many cases controller actions may result in a redirect
 * so this is a result object that implements all necessary properties of a HTTP redirect
 *
 * @api
 */
class Redirect extends \MegaCodex\Framework\View\Result\AbstractResult
{
    /**
     * @var string
     */
    protected $url;

    /**
     * @param string $url
     *
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    protected function render(\MegaCodex\Framework\App\Response $response)
    {
        $response->setRedirect($this->url);
        return $this;
    }
}
