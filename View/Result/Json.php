<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\View\Result;

class Json extends \MegaCodex\Framework\View\Result\AbstractResult
{
    /**
     * @var string
     */
    protected $json;

    /**
     * Set json data
     *
     * @param mixed $data
     * @param boolean $cycleCheck Optional; whether or not to check for object recursion; off by default
     * @param array $options Additional options used during encoding
     *
     * @return $this
     */
    public function setData($data, $cycleCheck = false, $options = [])
    {
        $this->json = \Zend\Json\Json::encode($data, $cycleCheck, $options);
        return $this;
    }

    /**
     * @param string $jsonData
     *
     * @return $this
     */
    public function setJsonData($jsonData)
    {
        $this->json = (string)$jsonData;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    protected function render(\MegaCodex\Framework\App\Response $response)
    {
        $response->setHeader("Content-Type", "application/json", true);
        $response->setBody($this->json);
        return $this;
    }
}
