<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\View\Result;

/**
 * An abstraction of result that controller actions must return
 * The point of this kind of object is to encapsulate all information/objects relevant to the result
 * and be able to set it to the HTTP response
 *
 * @api
 */
interface ResultInterface
{
    /**
     * @param int $httpCode
     *
     * @return $this
     */
    public function setHttpResponseCode($httpCode);

    /**
     * Set a header
     * If $replace is true, replaces any headers already defined with that
     * $name.
     *
     * @param string $name
     * @param string $value
     * @param boolean $replace
     *
     * @return $this
     */
    public function setHeader($name, $value, $replace = false);

    /**
     * Render result and set to response
     *
     * @param ResponseInterface $response
     *
     * @return $this
     */
    public function renderResult(\MegaCodex\Framework\App\Response $response);
}
