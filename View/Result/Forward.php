<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\View\Result;

class Forward extends AbstractResult
{
    /**
     * @var \MegaCodex\Framework\App\Request
     */
    protected $request;

    /**
     * @var string
     */
    protected $module;

    /**
     * @var string
     */
    protected $controller;

    /**
     * @var array
     */
    protected $params = [];

    /**
     * @param \MegaCodex\Framework\App\Request $request
     */
    public function __construct(\MegaCodex\Framework\App\Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param string $module
     *
     * @return $this
     */
    public function setModule($module)
    {
        $this->module = $module;
        return $this;
    }

    /**
     * @param string $controller
     *
     * @return $this
     */
    public function setController($controller)
    {
        $this->controller = $controller;
        return $this;
    }

    /**
     * @param array $params
     *
     * @return $this
     */
    public function setParams(array $params)
    {
        $this->params = $params;
        return $this;
    }

    /**
     * @param string $action
     *
     * @return $this
     */
    public function forward($action)
    {
        if (!empty($this->params)) {
            $this->request->setParams($this->params);
        }

        if (!empty($this->controller)) {
            $this->request->setControllerName($this->controller);

            // Module should only be reset if controller has been specified
            if (!empty($this->module)) {
                $this->request->setModuleName($this->module);
            }
        }

        $this->request->setActionName($action);
        $this->request->setDispatched(false);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    protected function render(\MegaCodex\Framework\App\Response $response)
    {
        return $this;
    }
}
