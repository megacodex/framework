<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\View\Result;

/**
 * A generic layout response can be used for rendering any kind of layout
 * So it comprises a response body from the layout elements it has and sets it to the HTTP response
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 *
 * @api
 */
class Layout extends \MegaCodex\Framework\View\Result\AbstractResult
{
    /**
     * @var \MegaCodex\Framework\View\Layout
     */
    protected $layout;

    public function __construct(
        \MegaCodex\Framework\View\Context $context,
        \MegaCodex\Framework\View\Layout $layout
    ) {
        parent::__construct($context);
        $this->layout = $layout;
    }

    /**
     * {@inheritdoc}
     */
    protected function render(\MegaCodex\Framework\App\Response $response)
    {
        $output = $this->layout->getOutput();
        var_dump($output);
        die();
        $this->translateInline->processResponseBody($output);
        $response->appendBody($output);
        return $this;
    }
}
