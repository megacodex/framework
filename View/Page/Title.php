<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\View\Page;

/**
 * Page title
 *
 * @api
 */
class Title
{
    /**
     * Default title glue
     */
    const TITLE_GLUE = " / ";

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $appConfig;

    /**
     * @var string[]
     */
    private $prependedValues = [];

    /**
     * @var string[]
     */
    private $appendedValues = [];

    /**
     * @var string
     */
    private $textValue;

    /**
     * @param App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \MegaCodex\Framework\App\Config\AppConfig $appConfig
    ) {
        $this->appConfig = $appConfig;
    }

    /**
     * Set page title
     *
     * @param string $title
     *
     * @return $this
     */
    public function set($title)
    {
        $this->textValue = $title;
        return $this;
    }

    /**
     * Retrieve title element text (encoded)
     *
     * @return string
     */
    public function get()
    {
        return join(self::TITLE_GLUE, $this->build());
    }

    /**
     * Same as getTitle(), but return only first item from chunk
     *
     * @return mixed
     */
    public function getShort()
    {
        $title = $this->build();
        return reset($title);
    }

    /**
     * Same as getShort(), but return title without prefix and suffix
     *
     * @return mixed
     */
    public function getShortHeading()
    {
        $title = $this->build(false);
        return reset($title);
    }

    /**
     * @param bool $withConfigValues
     *
     * @return array
     */
    protected function build($withConfigValues = true)
    {
        return array_merge(
            $this->prependedValues,
            [$withConfigValues ? $this->addConfigValues($this->textValue) : $this->textValue],
            $this->appendedValues
        );
    }

    /**
     * @param string $title
     *
     * @return string
     */
    protected function addConfigValues($title)
    {
        $preparedTitle = $this->appConfig->getData("design/head/title_prefix")
                         . " " . $title . " " . $this->appConfig->getData("design/head/title_suffix");
        return trim($preparedTitle);
    }

    /**
     * Retrieve default title text
     *
     * @return string
     */
    public function getDefault()
    {
        $defaultTitle = $this->appConfig->getData("design/head/default_title");
        return $this->addConfigValues($defaultTitle);
    }

    /**
     * @param string $suffix
     *
     * @return void
     */
    public function append($suffix)
    {
        $this->appendedValues[] = $suffix;
    }

    /**
     * @param string $prefix
     *
     * @return void
     */
    public function prepend($prefix)
    {
        array_unshift($this->prependedValues, $prefix);
    }

    /**
     * Unset title
     *
     * @return void
     */
    public function unsetValue()
    {
        $this->textValue = null;
    }
}
