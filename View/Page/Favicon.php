<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\View\Page;

/**
 * Favicon implementation
 */
class Favicon
{
    /**
     * @var string
     */
    protected $faviconFile;

    /**
     * @var \MegaCodex\Framework\App\Config\ScopeConfigInterface
     */
    protected $appConfig;

    /**
     * @var \MegaCodex\Framework\Filesystem\Directory\ReadInterface
     */
    protected $mediaDirectory;

    /**
     * @param \MegaCodex\Store\Model\StoreManagerInterface $storeManager
     * @param \MegaCodex\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \MegaCodex\MediaStorage\Helper\File\Storage\Database $fileStorageDatabase
     * @param \MegaCodex\Framework\Filesystem $filesystem
     */
    public function __construct(
        \MegaCodex\Framework\App\Config\AppConfig $appConfig,
        \MegaCodex\Framework\App\Filesystem $filesystem
    ) {
        $this->appConfig = $appConfig;
        $this->mediaDirectory = $filesystem->getDirectoryRead(\MegaCodex\Framework\App\Filesystem\DirectoryList::MEDIA);
    }

    /**
     * @return string
     */
    public function getFaviconFile()
    {
        if (null === $this->faviconFile) {
            $this->faviconFile = $this->prepareFaviconFile();
        }
        return $this->faviconFile;
    }

    /**
     * @return string
     */
    public function getDefaultFavicon()
    {
        return "MegaCodex_Theme::favicon.ico";
    }

    /**
     * @return string
     */
    protected function prepareFaviconFile()
    {
        $path = $this->appConfig->getValue("design/head/shortcut_icon");
        $faviconUrl = $this->appConfig->getBaseUrl(\MegaCodex\Framework\UrlInterface::URL_TYPE_MEDIA) . $path;

        if ($scopeConfig !== null && $this->checkIsFile($path)) {
            return $faviconUrl;
        }

        return false;
    }

    /**
     * If DB file storage is on - find there, otherwise - just file_exists
     *
     * @param string $filename relative file path
     *
     * @return bool
     */
    protected function checkIsFile($filename)
    {
        return $this->mediaDirectory->isFile($filename);
    }
}
