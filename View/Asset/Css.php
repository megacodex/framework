<?php
/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\View\Asset;

class Css extends \MegaCodex\Framework\View\Asset\AbstractAsset
{
    /**
     * @var \MegaCodex\Framework\App\Config\ModuleConfig
     */
    protected $modules;

    /**
     * @var \Leafo\ScssPhp\Compiler
     */
    protected $cssProcessor;

    /**
     * @param \MegaCodex\Framework\App\ObjectManager $objectManager
     * @param \MegaCodex\Framework\App\Filesystem $filesystem
     * @param \MegaCodex\Logger\Logger $logger
     * @param \MegaCodex\Framework\App\Bootstrap $bootstrap
     * @param \MegaCodex\Framework\App\Config\AppConfig $config
     * @param \MegaCodex\Framework\App\Config\ModuleConfig $modules
     * @param \Leafo\ScssPhp\Compiler $cssProcessor
     */
    public function __construct(
        \MegaCodex\Framework\App\ObjectManager $objectManager,
        \MegaCodex\Framework\App\Filesystem $filesystem,
        \MegaCodex\Logger\Logger $logger,
        \MegaCodex\Framework\App\Config\AppConfig $config,
        \MegaCodex\Framework\App\Config\ModuleConfig $modules,
        \Leafo\ScssPhp\Compiler $cssProcessor
    ) {
        parent::__construct($objectManager, $filesystem, $logger, $config);
        $this->modules = $modules->getModules();
        $this->cssProcessor = $cssProcessor;
        $this->setCssProcessorOptions();
        $this->_recompileFile = $config->getRecompileCss();
    }

    public function publish()
    {
        if ($this->shouldWrite()) {
            if (!$this->_moduleWriter->isExist($this->_modulePath)) {
                $this->_modulePath = rtrim($this->_modulePath, "css") . "scss";
                $this->handleSource();
                if (!$this->_moduleWriter->isExist($this->_modulePath)) {
                    throw new \InvalidArgumentException("Requested file \""
                                                        . $this->_modulePath . "\" dosen't exist.");
                }

                $this->_modulePath = $this->_moduleWriter
                    ->getAbsolutePath($this->_modulePath);
                gc_disable();
                $css = $this->cssProcessor->compile("@scssphp-import-once \"" . $this->_modulePath . "\";");
                gc_enable();

                $this->_staticReader->writeFile($this->_staticPath, $css, "w");
                return true;
            }
            return parent::publish();
        }
        return true;
    }

    private function handleSource()
    {
        $this->_modulePath = rtrim($this->_staticPath, "css") . "scss";

        foreach ($this->modules as $module => $modulePath) {
            $modulePath .= "web" . DIRECTORY_SEPARATOR . "css" . DIRECTORY_SEPARATOR;
            $files = $this->_moduleWriter->readRecursively($modulePath);

            foreach ($files as $file) {
                $dest = str_replace($modulePath, $module . DIRECTORY_SEPARATOR, $file);
                $dest = $this->_themePath . "css" . DIRECTORY_SEPARATOR . $dest;

                if ($this->_moduleWriter->isFile($file)) {
                    $this->_moduleWriter->copyFile($file, $dest, $this->_staticReader);
                }
            }
        }
        if (strpos($this->_modulePath, "/css/styles.scss") > 0) {
            $css = [[], [], []];
            foreach ($this->modules as $module => $modulePath) {
                $modulePath .= "web" . DIRECTORY_SEPARATOR . "css" . DIRECTORY_SEPARATOR;
                if ($this->_moduleWriter->isExist($modulePath . "_variable.scss")) {
                    $css[0][] = "@scssphp-import-once \"" . $module . "/_variable.scss\"";
                }
                if ($this->_moduleWriter->isExist($modulePath . "_common.scss")) {
                    $css[1][] = "@scssphp-import-once \"" . $module . "/_common.scss\"";
                }
                if ($this->_moduleWriter->isExist($modulePath . "_module.scss")) {
                    $css[2][] = "@scssphp-import-once \"" . $module . "/_module.scss\"";
                }
            }

            $css = join(";\r\n",
                        array_map(function ($css) {
                            return join(";\r\n", $css);
                        },
                            $css)) . ";";
            $this->_staticReader->writeFile($this->_modulePath, $css, "w");
        }

        $this->_moduleWriter = $this->_staticReader;
    }

    protected function setCssProcessorOptions()
    {
        if ($this->_config->isDeveloperMode()) {
            $this->cssProcessor->setFormatter(\Leafo\ScssPhp\Formatter\Expanded::class);
            $this->cssProcessor->setLineNumberStyle(\Leafo\ScssPhp\Compiler::LINE_COMMENTS);
            $this->cssProcessor->setSourceMap(\Leafo\ScssPhp\Compiler::SOURCE_MAP_INLINE);
            $this->cssProcessor->setSourceMapOptions([
                                                         "sourceRoot"        => "",
                                                         "sourceMapFilename" => null,
                                                         "sourceMapURL"      => null,
                                                         "sourceMapWriteTo"  => null,
                                                         "outputSourceFiles" => false,
                                                         "sourceMapRootpath" => "",
                                                         "sourceMapBasepath" => "/",
                                                     ]);
        } else {
            $this->cssProcessor->setFormatter(\Leafo\ScssPhp\Formatter\Crunched::class);
        }
        $this->cssProcessor->setImportPaths("");
    }
}
