<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\View\Asset;

class AbstractAsset implements \MegaCodex\Framework\View\AssetInterface
{

    /**
     * @var \MegaCodex\Framework\App\ObjectManager
     */
    protected $_objectManager;

    /**
     * @var \MegaCodex\Framework\Filesystem\Directory\WriteInterface
     */
    protected $_staticReader;

    /**
     * @var \MegaCodex\Framework\Filesystem\Directory\WriteInterface
     */
    protected $_moduleWriter;

    /**
     * @var \MegaCodex\Logger\Logger
     */
    protected $_logger;

    /**
     * @var string
     */
    protected $_staticPath;

    /**
     * @var string
     */
    protected $_modulePath;

    /**
     * @var \MegaCodex\Framework\App\Config\AppConfig
     */
    protected $_config;

    /**
     * @var bool
     */
    protected $_recompileFile;

    /**
     * @param \MegaCodex\Framework\App\ObjectManager $objectManager
     * @param \MegaCodex\Framework\App\Filesystem $filesystem
     * @param \MegaCodex\Logger\Logger $logger
     * @param \MegaCodex\Framework\App\Bootstrap $bootstrap
     * @param \MegaCodex\Framework\App\Config\AppConfig $config
     */
    public function __construct(
        \MegaCodex\Framework\App\ObjectManager $objectManager,
        \MegaCodex\Framework\App\Filesystem $filesystem,
        \MegaCodex\Logger\Logger $logger,
        \MegaCodex\Framework\App\Config\AppConfig $config
    ) {
        $this->_objectManager = $objectManager;
        $this->_staticReader = $filesystem->getDirectoryWrite(
            \MegaCodex\Framework\App\Filesystem\DirectoryList::STATIC_VIEW
        );
        $this->_moduleWriter = $filesystem->getDirectoryWrite(
            \MegaCodex\Framework\App\Filesystem\DirectoryList::ROOT
        );
        $this->_logger = $logger;
        $this->_config = $config;
        $this->_recompileFile = false;
    }

    public function getPath()
    {
        return $this->_staticReader->getAbsolutePath($this->_staticPath);
    }

    public function publish()
    {
        if ($this->shouldWrite()) {
            $this->_moduleWriter->copyFile(
                $this->_modulePath,
                $this->_staticPath,
                $this->_staticReader
            );
            return true;
        }
        return true;
    }

    public function setPaths($filePath, $module, $theme)
    {
        $this->_themePath = $theme;
        $this->_staticPath = $theme . $filePath;
        $this->_modulePath = $module . "web" . DIRECTORY_SEPARATOR . $filePath;
    }

    protected function shouldWrite()
    {
        if ($this->_staticReader->isExist($this->_staticPath) && !$this->_recompileFile) {
            return false;
        }
        return true;
    }
}
