<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\View\Theme;

class Theme
{
    private $id;
    private $path;
    private $dir;

    public function __construct(string $name)
    {
        $this->id = $name;
        $this->path = join("/", explode("_", strtolower($this->id))) . DIRECTORY_SEPARATOR;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUrlPath()
    {
        return $this->_path;
    }

    public function getThemeDir()
    {
        return $this->dir;
    }
}
