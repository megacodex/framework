<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\View\Theme;

class Resolver
{
    private $themes;
    private $current;

    const DEFAULT_THEME = "MegaCodex_Blank";

    /**
     * @var \MegaCodex\Framework\App\ObjectManager
     */
    protected $objectManager;

    public function __construct(
        \MegaCodex\Framework\View\Context $context
    ) {
        $this->objectManager = $context->getObjectManager();
        $this->loadData();
    }

    protected function loadData()
    {
        $this->current = $this->addTheme(self::DEFAULT_THEME);
    }

    public function getAllTheme()
    {
        return $this->themes;
    }

    public function get()
    {
        return $this->current;
    }

    /**
     * @return \MegaCodex\Framework\View\Theme\Theme
     */
    protected function addTheme($name)
    {
        $theme = $this->objectManager->create(
            \MegaCodex\Framework\View\Theme\Theme::class,
            ["name" => $name]
        );
        $this->themes[] = $theme;
        return $theme;
    }
}
