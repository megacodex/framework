<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\View;

/**
 * Application Runtime Context
 *
 * @todo Reduce fields number
 * @todo Reduce class dependencies
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @api
 */
class Context extends \MegaCodex\Framework\App\Config\Context
{
    public function __construct(
        \MegaCodex\Framework\App\ObjectManager $objectManager,
        \MegaCodex\Framework\App\Request $request,
        \MegaCodex\Framework\App\Filesystem $fileSystem
    ) {
        parent::__construct($objectManager, $request, $fileSystem);
    }
}
