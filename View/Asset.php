<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\View;

class Asset
{
    /**
     * @var \MegaCodex\Framework\App\Config\ModuleConfig
     */
    protected $_moduleConfig;

    /**
     * @var \MegaCodex\Framework\App\ObjectManager
     */
    protected $_objectManager;

    const ASSET_HANDLER_CSS = \MegaCodex\Framework\View\Asset\Css::class;
    const ASSET_HANDLER_JS = \MegaCodex\Framework\View\Asset\Js::class;
    const ASSET_HANDLER_DEFAULT = \MegaCodex\Framework\View\Asset\AbstractAsset::class;

    public function __construct(
        \MegaCodex\Framework\App\ObjectManager $objectManager,
        \MegaCodex\Framework\App\Config\ModuleConfig $moduleConfig
    ) {
        $this->_objectManager = $objectManager;
        $this->_moduleConfig = $moduleConfig;
    }

    /**
     * @param string $file
     * @param array $params
     *
     * @return \MegaCodex\Framework\View\AssetInterface
     */
    public function createAsset(string $filePath, string $module, string $theme)
    {
        if (empty($module)) {
            $module = \MegaCodex\Framework\App\Config\ModuleConfig::DEFAUL_MODULE;
        }

        $module = $this->getModulePath($module);
        $type = explode(".", $filePath);
        $handler = null;
        switch (array_pop($type)) {
            case "css":
                $handler = $this->_objectManager->get(self::ASSET_HANDLER_CSS);
                break;

            case "js":
                $handler = $this->_objectManager->get(self::ASSET_HANDLER_JS);
                break;

            default:
                $handler = $this->_objectManager->get(self::ASSET_HANDLER_DEFAULT);
                break;
        }
        $handler->setPaths($filePath, $module, $theme);
        return $handler;
    }

    private function getModulePath($module)
    {
        $module = $this->_moduleConfig->getModule($module);
        if (empty($module)) {
            throw new \MegaCodex\Framework\Exception\NotFoundException(
                new \MegaCodex\Framework\Phrase("Requested Module Dosen't Exist")
            );
        }
        return $module;
    }
}
