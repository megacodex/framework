<?php
/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\App;

class StaticResource extends \MegaCodex\Framework\App\Http
{
    private $_asset;
    private $_moduleConfig;
    private $_resultForward;

    public function __construct(
        ObjectManager $objectManager,
        Request $request,
        Response $response,
        Filesystem $filesystem,
        \MegaCodex\Logger\Logger $logger,
        \MegaCodex\Framework\View\Asset $asset,
        \MegaCodex\Framework\App\Config\ModuleConfig $moduleConfig
    ) {
        parent::__construct($objectManager, $request, $response, $filesystem, $logger);
        $this->_asset = $asset;
        $this->_moduleConfig = $moduleConfig;
    }

    public function launch()
    {
        $params = $this->getParams();
        $asset = $this->_asset->createAsset($params["file"], $params["module"], $params["theme"]);
        if ($asset->publish()) {
            $this->_response->setFilePath($asset->getPath());
        } else {
            $this->response->setHttpResponseCode(404);
        }
        return $this->_response;
    }

    public function catchException(
        \MegaCodex\Framework\App\Bootstrap $bootstrap,
        \Exception $exception
    ) {
        $this->_logger->critical($exception->getMessage());
        if ($bootstrap->isDeveloperMode()) {
            $this->_response->setHttpResponseCode(404);
            $this->_response->setHeader("Content-Type", "text/plain");
            $this->_response->setBody($exception->getMessage() . "\n" . $exception->getTraceAsString());
            $this->_response->sendResponse();
        } else {
            require $this->_filesystem
                ->getDirectoryRead(Filesystem\DirectoryList::PUB)
                ->getAbsolutePath("errors/404.php");
        }
        return true;
    }

    protected function getParams()
    {
        $path = ltrim($this->_request->getPathInfo(), "/");
        $parts = explode("/", $path, 6);
        unset($parts[0]);

        if (count($parts) < 4) {
            throw new \InvalidArgumentException("Requested path \"$path\" is wrong.");
        }
        $result = [];
        $result["theme"] = $parts[1] . "/" . $parts[2] . "/";
        unset($parts[1]);
        unset($parts[2]);
        // TODO integrate Module
        if (count($parts) >= 4 && $this->_moduleConfig->has($parts[3])) {
            $result["module"] = $parts[3];
            unset($parts[3]);
        } else {
            $result["module"] = "";
        }

        $result["file"] = join("/", $parts);
        return $result;
    }
}
