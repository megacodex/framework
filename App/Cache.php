<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\App;

class Cache extends \Zend\Cache\Storage\Adapter\Filesystem
{
    public function __construct(
        \MegaCodex\Framework\App\Config\Context $context,
        \MegaCodex\Framework\Filesystem\DriverPool $fileSystemDriver
    ) {
        $cachePath = $context->getFileSystem()->getDirPath(\MegaCodex\Framework\App\Filesystem\DirectoryList::CACHE);
        $fileSystemDriver->getDriver(\MegaCodex\Framework\Filesystem\DriverPool::FILE)->createDirectory($cachePath);

        parent::__construct([
                                "namespace_separator" => "_",
                                "dir_level"           => 2,
                                "namespace"           => "MCF_Cache",
                                "cache_dir"           => $cachePath,
                            ]);
    }

    public function load($cacheId)
    {
        return $this->getItem($this->_unifyId($cacheId));
    }

    public function save($data, $cacheId, $tags = [])
    {
        $cacheId = $this->_unifyId($cacheId);
        $this->setItem($cacheId, $data);
        $this->setTags($cacheId, $this->_unifyIds($tags));
    }

    protected function _unifyId($identifier)
    {
        return strtoupper($identifier);
    }

    /**
     * Retrieve multiple unified identifiers
     *
     * @param array $ids
     *
     * @return array
     */
    protected function _unifyIds(array $ids)
    {
        foreach ($ids as $key => $value) {
            $ids[$key] = $this->_unifyId($value);
        }
        return $ids;
    }
}
