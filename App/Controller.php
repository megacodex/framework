<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\App;

class Controller implements \MegaCodex\Framework\App\ControllerInterface
{

    /**
     * @var \MegaCodex\Framework\App\Router
     */
    protected $_router;

    /**
     * @var \\MegaCodex\Framework\App\Response
     */
    protected $_response;

    /**
     * @var \MegaCodex\Framework\App\Request
     */
    protected $_request;

    /**
     * @param \MegaCodex\Framework\App\Router $router
     * @param \MegaCodex\Framework\App\Request $request
     * @param \MegaCodex\Framework\App\Response $response
     */
    public function __construct(
        \MegaCodex\Framework\App\Router $router,
        \MegaCodex\Framework\App\Request $request,
        \MegaCodex\Framework\App\Response $response
    ) {
        $this->_router = $router;
        $this->_request = $request;
        $this->_response = $response;
    }

    public function dispatch()
    {
        $routingCycleCounter = 0;
        $result = null;
        while (!$this->_request->isDispatched() && $routingCycleCounter++ < 100) {
            $actionInstance = $this->_router->match();
            if ($actionInstance) {
                $this->_request->setDispatched(true);
                $this->_response->setNoCacheHeaders();
                $result = $actionInstance->dispatch($this->_request);
                break;
            }
        }

        return $result;
    }
}
