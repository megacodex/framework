<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\App;

class Http implements \MegaCodex\Framework\AppInterface
{
    /**
     * @var \MegaCodex\Framework\App\ObjectManager
     */
    protected $_objectManager;

    /**
     * @var \MegaCodex\Framework\App\Request
     */
    protected $_request;

    /**
     * @var \MegaCodex\Framework\App\Response
     */
    protected $_response;

    /**
     * @var \MegaCodex\Framework\App\Filesystem
     */
    protected $_filesystem;

    /**
     * @var \MegaCodex\Logger\Logger
     */
    protected $_logger;

    public function __construct(
        \MegaCodex\Framework\App\ObjectManager $objectManager,
        \MegaCodex\Framework\App\Request $request,
        \MegaCodex\Framework\App\Response $response,
        \MegaCodex\Framework\App\Filesystem $filesystem,
        \MegaCodex\Logger\Logger $logger
    ) {
        $this->_objectManager = $objectManager;
        $this->_request = $request;
        $this->_response = $response;
        $this->_filesystem = $filesystem;
        $this->_logger = $logger;
    }

    public function launch()
    {
        $this->_request->setDispatched(false);
        $result = $this->_objectManager
            ->get(\MegaCodex\Framework\App\Controller::class)
            ->dispatch($this->_request);

        if ($result instanceof \MegaCodex\Framework\Controller\Result) {
            $result->renderResult($this->_response);
        } elseif ($result instanceof \MegaCodex\Framework\App\ResponseInterface) {
            $this->_response = $result;
        } else {
            throw new \InvalidArgumentException("Invalid return type");
        }
        return $this->_response;
    }

    /**
     * {@inheritdoc}
     */
    public function catchException(
        \MegaCodex\Framework\App\Bootstrap $bootstrap,
        \Exception $exception
    ) {
        $result = $this->handleDeveloperMode($bootstrap, $exception)
                  || $this->handleSessionException($exception)
                  || $this->handleInitException($exception)
                  || $this->handleGenericReport($bootstrap, $exception);
        return $result;
    }

    /**
     * Error handler for developer mode
     *
     * @param \MegaCodex\Framework\App\Bootstrap $bootstrap
     * @param \Exception $exception
     *
     * @return bool
     */
    private function handleDeveloperMode(
        \MegaCodex\Framework\App\Bootstrap $bootstrap,
        \Exception $exception
    ) {
        if ($bootstrap->isDeveloperMode()) {
            $this->_response->setHttpResponseCode(500);
            $this->_response->setHeader("Content-Type", "text/plain");
            $this->_response->setBody($this->buildContentFromException($exception));
            $this->_response->sendResponse();
            return true;
        }
        return false;
    }

    /**
     * Build content based on an exception
     *
     * @param \Exception $exception
     *
     * @return string
     */
    private function buildContentFromException(\Exception $exception)
    {
        /** @var \Exception[] $exceptions */
        $exceptions = [];
        do {
            $exceptions[] = $exception;
        } while ($exception = $exception->getPrevious());

        $buffer = sprintf("%d exception(s):\n", count($exceptions));

        foreach ($exceptions as $index => $exception) {
            $buffer .= sprintf("Exception #%d (%s): %s\n", $index, get_class($exception), $exception->getMessage());
        }

        foreach ($exceptions as $index => $exception) {
            $buffer .= sprintf(
                "\nException #%d (%s): %s\n%s\n",
                $index,
                get_class($exception),
                $exception->getMessage(),
                $exception->getTraceAsString()
            );
        }

        return $buffer;
    }

    /**
     * Handler for session errors
     *
     * @param \Exception $exception
     *
     * @return bool
     */
    private function handleSessionException(\Exception $exception)
    {
        if ($exception instanceof \Magento\Framework\Exception\SessionException) {
            $this->_response->setRedirect($this->_request->getDistroBaseUrl());
            $this->_response->sendHeaders();
            return true;
        }
        return false;
    }

    /**
     * @param \Exception $exception
     *
     * @return boolean
     */
    private function handleInitException(\Exception $exception)
    {
        if ($exception instanceof \MegaCodex\Framework\Exception\InitException) {
            $this->_logger->critical($exception);
            require $this->_filesystem->getDirectoryRead(DirectoryList::PUB)->getAbsolutePath("errors/404.php");
            return true;
        }
        return false;
    }

    /**
     * Handle for any other errors
     *
     * @param Bootstrap $bootstrap
     * @param \Exception $exception
     *
     * @return bool
     */
    private function handleGenericReport(Bootstrap $bootstrap, \Exception $exception)
    {
        $reportData = [$exception->getMessage(), $exception->getTraceAsString()];
        $params = $bootstrap->getParams();
        if (isset($params["REQUEST_URI"])) {
            $reportData["url"] = $params["REQUEST_URI"];
        }
        if (isset($params["SCRIPT_NAME"])) {
            $reportData["script_name"] = $params["SCRIPT_NAME"];
        }
        require $this->_filesystem
            ->getDirectoryRead(\MegaCodex\Framework\App\Filesystem\DirectoryList::PUB)
            ->getAbsolutePath("errors/report.php");
        return true;
    }
}
