<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\App;

class ActionFactory
{
    /**
     * @var \MegaCodex\Framework\App\ObjectManager
     */
    protected $_objectManager;

    /**
     * @param \MegaCodex\Framework\App\ObjectManager $objectManager
     */
    public function __construct(\MegaCodex\Framework\App\ObjectManager $objectManager)
    {
        $this->_objectManager = $objectManager;
    }

    /**
     * Create action
     *
     * @param string $actionName
     *
     * @return ActionInterface
     * @throws \InvalidArgumentException
     */
    public function create($actionName)
    {
        if (!is_subclass_of($actionName, "\MegaCodex\Framework\App\Action\ActionInterface")) {
            throw new \InvalidArgumentException("Invalid action name provided");
        }
        return $this->_objectManager->create($actionName);
    }
}
