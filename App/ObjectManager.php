<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\App;

class ObjectManager extends \Zend\Di\Di
{
    /**
     * Filesystem directory list
     *
     * @var \MegaCodex\Framework\App\Filesystem\DirectoryList
     */
    protected $_directoryList;

    /**
     * Filesystem driver pool
     *
     * @var \MegaCodex\Framework\Filesystem\DriverPool
     */
    protected $_driverPool;

    /**
     * Configuration file pool
     *
     * @var \\MegaCodex\Framework\App\Config\DefaultPool
     */
    protected $_configFilePool;

    /**
     * @param \Zend\Di\DefinitionList $definitions
     * @param \Zend\Di\InstanceManager $instanceManager
     * @param \Zend\Di\Config $config
     */
    public function __construct($rootDir)
    {
        parent::__construct();

        $this->instanceManager->addSharedInstance($this, "MegaCodex\Framework\App\ObjectManager");
        $this->_directoryList = $this->get("MegaCodex\Framework\App\Filesystem\DirectoryList", ["root" => $rootDir, "config" => []]);
        $this->instanceManager->addSharedInstance($this->_directoryList, "MegaCodex\Framework\App\Filesystem\DirectoryList");
        $this->_driverPool = $this->get("MegaCodex\Framework\Filesystem\DriverPool");
        $this->_configFilePool = $this->get("MegaCodex\Framework\App\Config\DefaultPool");

        $config = $this->get("MegaCodex\Framework\App\Config\DiConfig");
        $this->configure($config);
    }

    /**
     * Retrieve a new instance of a class
     * Forces retrieval of a discrete instance of the given class, using the
     * constructor parameters provided.
     *
     * @param mixed $name Class name or service alias
     * @param array $params Parameters to pass to the constructor
     * @param bool $isShared
     *
     * @return object|null
     * @throws Exception\ClassNotFoundException
     * @throws Exception\RuntimeException
     */
    public function create($name, array $params = [])
    {
        $name = trim($name, "\\");
        return $this->newInstance($name, $params, false);
    }

    public function get($name, array $params = [])
    {
        $name = trim($name, "\\");
        return parent::get($name, $params);
    }
}
