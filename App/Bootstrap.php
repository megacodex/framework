<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\App;

class Bootstrap
{
    /**
     * @var array
     */
    private $_server;

    /**
     * @var string
     */
    private $_rootDir;

    /**
     * @var \MegaCodex\Framework\App\ObjectManager
     */
    private $_objectManager;

    public function __construct(
        \MegaCodex\Framework\App\ObjectManager $objectManager,
        $rootDir,
        array $initParams
    ) {
        $this->_rootDir = $rootDir;
        $this->_server = $initParams;
        $this->_objectManager = $objectManager;
        $this->_objectManager->instanceManager()
                             ->addSharedInstance($this, "MegaCodex\Framework\App\Bootstrap");
    }

    /**
     * @param type $rootDir
     * @param array $initParams
     * @param \MegaCodex\Framework\App\ObjectManagerFactory $factory
     *
     * @return \self
     */
    public static function create(
        $rootDir,
        array $initParams,
        \MegaCodex\Framework\App\ObjectManager $objectManager = null
    ) {
        if ($objectManager === null) {
            $objectManager = new \MegaCodex\Framework\App\ObjectManager($rootDir);
        }
        return new self($objectManager, $rootDir, $initParams);
    }

    public function getObjectManager()
    {
        return $this->_objectManager;
    }

    /**
     * Factory method for creating application instances
     *
     * @param string $type
     * @param array $arguments
     *
     * @return \MegaCodex\Framework\AppInterface
     * @throws \InvalidArgumentException
     */
    public function createApplication($type, $arguments = [])
    {
        try {
            $application = $this->_objectManager->create($type, $arguments);
            if (!($application instanceof \MegaCodex\Framework\AppInterface)) {
                throw new \InvalidArgumentException("The provided class doesn't implement AppInterface: {$type}");
            }
            return $application;
        } catch (\Exception $e) {
            $this->terminate($e);
        }
    }

    /**
     * Runs an application
     *
     * @param \MegaCodex\Framework\AppInterface $application
     *
     * @return void
     */
    public function run(\MegaCodex\Framework\AppInterface $application)
    {
        try {
            try {
                $response = $application->launch();
                $response->sendResponse();
            } catch (\Exception $e) {
                if (!$application->catchException($this, $e)) {
                    throw $e;
                }
            }
        } catch (\Exception $e) {
            $this->terminate($e);
        }
    }

    public function getParams($name = null)
    {
        if (empty($name)) {
            return $this->_server;
        }
        return isset($this->_server[$name]) ?: false;
    }

    /**
     * Checks whether developer mode is set in the initialization parameters
     *
     * @return bool
     */
    public function isDeveloperMode()
    {
        $config = $this->_objectManager->get("MegaCodex\Framework\App\Config\AppConfig");

        if (isset($this->_server["APP_MODE"])) {
            $config->setState($this->_server["APP_MODE"]);
        }

        return $config->isDeveloperMode();
    }

    /**
     * Display an exception and terminate program execution
     *
     * @param \Exception $e
     *
     * @return void
     * @SuppressWarnings(PHPMD.ExitExpression)
     */
    protected function terminate(\Exception $e)
    {
        if ($this->isDeveloperMode()) {
            print "<pre>" . $e . "</pre>";
        } else {
            $message = "An error has happened during application run. See exception log for details.\n";
            try {
                if (!$this->_objectManager) {
                    throw new \DomainException();
                }
                $this->_objectManager->get("Psr\Log\LoggerInterface")->critical($e);
            } catch (\Exception $e) {
                $message .= "Could not write error message to log. Please use developer mode to see the message.\n";
            }
            print $message;
        }
        exit(1);
    }
}
