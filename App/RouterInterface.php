<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\App;

interface RouterInterface
{
    /**
     * Match application action by request
     *
     * @return \MegaCodex\Framework\App\Action\Action
     */
    public function match();
}
