<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\App\Action;

interface ActionInterface
{
    /**
     * Dispatch request
     *
     * @param \MegaCodex\Framework\App\Request $request
     *
     * @return \MegaCodex\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \MegaCodex\Framework\Exception\NotFoundException
     */

    public function dispatch(\MegaCodex\Framework\App\Request $request);
}
