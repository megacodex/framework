<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\App\Action;

class Context extends \MegaCodex\Framework\App\Config\Context
{
    /**
     * @var \MegaCodex\Framework\App\Request
     */
    protected $_request;

    /**
     * @var \MegaCodex\Framework\App\Response
     */
    protected $_response;

    /**
     * @var \MegaCodex\Framework\App\ObjectManager
     */
    protected $_objectManager;

    /**
     * @var \MegaCodex\Framework\App\Filesystem
     */
    protected $_fileSystem;

    /**
     * @param \MegaCodex\Framework\App\ObjectManager $objectManager
     * @param \MegaCodex\Framework\App\Request $request
     * @param \MegaCodex\Framework\App\Response $response
     * @param \MegaCodex\Framework\App\Filesystem $fileSystem
     */
    public function __construct(
        \MegaCodex\Framework\App\ObjectManager $objectManager,
        \MegaCodex\Framework\App\Request $request,
        \MegaCodex\Framework\App\Response $response,
        \MegaCodex\Framework\App\Filesystem $fileSystem
    ) {
        $this->_objectManager = $objectManager;
        $this->_request = $request;
        $this->_response = $response;
        $this->_fileSystem = $fileSystem;
    }
}
