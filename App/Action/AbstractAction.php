<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\App\Action;

abstract class AbstractAction implements \MegaCodex\Framework\App\Action\ActionInterface
{
    /**
     * @var \MegaCodex\Framework\App\Request
     */
    protected $_request;

    /**
     * @var \MegaCodex\Framework\App\Response
     */
    protected $_response;

    /**
     * @var \MegaCodex\Framework\View\Result\ResultInterface
     */
    protected $_resultResponse;

    public function __construct(
        \MegaCodex\Framework\App\Action\Context $context,
        \MegaCodex\Framework\View\Result\ResultInterface $resultResponse
    ) {
        $this->_request = $context->getRequest();
        $this->_response = $context->getResponse();
        $this->_resultResponse = $resultResponse;
    }

    /**
     * Dispatch request
     *
     * @param \MegaCodex\Framework\App\Request $request
     *
     * @return \MegaCodex\Framework\App\Response
     */
    public function dispatch(\MegaCodex\Framework\App\Request $request)
    {
        $result = null;
        if ($request->isDispatched()) {
            $result = $this->execute();
        }
        return $result ?: $this->_response;
    }

    /**
     * Retrieve request object
     *
     * @return \MegaCodex\Framework\App\Request
     */
    public function getRequest()
    {
        return $this->_request;
    }

    /**
     * Retrieve response object
     *
     * @return \MegaCodex\Framework\App\Response
     */
    public function getResponse()
    {
        return $this->_response;
    }

    protected function execute()
    {
        $this->_resultResponse->renderResult($this->_response);
    }
}
