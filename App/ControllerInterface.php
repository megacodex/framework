<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\App;

interface ControllerInterface
{
    /**
     * Dispatch application action
     *
     * @return \MegaCodex\Framework\App\ResponseInterface
     */
    public function dispatch();
}
