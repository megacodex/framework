<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\App\Config;

class DefaultPool
{
    const MODULE_CONFIG = "module_config";
    const APP_CONFIG = "app_env";

    private $_configFiles = [
        self::MODULE_CONFIG => "modules.php",
        self::APP_CONFIG    => "config.php",
    ];

    private $_configData;

    public function __construct()
    {
        $this->_configData = [];
        foreach ($this->_configFiles as $type => $file) {
            $file = BP . DIRECTORY_SEPARATOR . "app" . DIRECTORY_SEPARATOR . "etc" . DIRECTORY_SEPARATOR . $file;
            $this->_configData[$type] = include_once $file;
        }
    }

    public function getAppConfig($name = null)
    {
        return $this->_getConfig(self::APP_CONFIG, $name);
    }

    public function getModuleConfig($name = null)
    {
        return $this->_getConfig(self::MODULE_CONFIG, $name);
    }

    private function _getConfig($type, $name = null)
    {
        if (empty($name)) {
            return $this->_configData[$type];
        }
        return isset($this->_configData[$type][$name]) ? $this->_configData[$type][$name] : null;
    }
}
