<?php
/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\App\Config;

class DiConfig extends \Zend\Di\Config
{
    private $_objectManager;
    private $_moduleConfig;

    public function __construct(
        \MegaCodex\Framework\App\ObjectManager $objectManager,
        \MegaCodex\Framework\App\Config\ModuleConfig $moduleConfig
    ) {
        $this->_objectManager = $objectManager;
        $this->_moduleConfig = $moduleConfig;
        $options = $this->_generateDiConfig();

        parent::__construct($options);
    }

    private function _generateDiConfig()
    {
        $types = $this->_moduleConfig->getConfig(\MegaCodex\Framework\App\Config\ModuleConfig::CONFIG_TYPE_DI);
        $diConfigs = [];
        foreach ($types as $type) {
            foreach ($type as $config) {
                if ($config->getTag() == "type") {
                    $diConfig = [];
                    foreach ($config->getArguments() as $arguments) {
                        foreach ($arguments->getArgument() as $argument) {
                            $data = $this->_formatTypeConfig($argument);
                            $diConfig[$argument->getName()] = $data[$argument->getName()];
                        }
                    }

                    if (empty($diConfigs[$config->getName()])) {
                        $diConfigs[$config->getName()] = ["parameters" => []];
                    }
                    $diConfigs[$config->getName()]["parameters"] += $diConfig;
                }
            }
        }
        $diConfigs = ["instance" => $diConfigs];
        return $diConfigs;
    }

    private function _formatTypeConfig($data, $type = null)
    {
        $config = null;
        if (empty($type)) {
            $type = gettype($data);
        }

        switch ($type) {
            case("object"):
                if (empty($data)) {
                    break;
                }
                if ($data instanceof \MegaCodex\Framework\Data) {
                    $config[$data->getName()] = $this->_formatTypeConfig($data->getValue(), $data->getType());
                } else {
                    $config = $this->_objectManager->get(trim($data));
                }

                break;
            case("const"):
                $data = trim($data);
                if (empty($data)) {
                    break;
                }
                if (strpos($data, "::") > -1) {
                    $data = "\\" . trim($data, "\\");
                }
                if (!defined($data)) {
                    throw new \InvalidArgumentException("Undefined Constant \"" . $data . "\".");
                }
                $config = constant($data);
                break;
            case("array"):
                $config = [];
                if (empty($data)) {
                    break;
                }

                foreach ($data as $key => $value) {
                    $config += $this->_formatTypeConfig($value);
                }

                break;
            case("method"):
                if (empty($data)) {
                    break;
                }
                $config = [];
                foreach ($data as $key => $value) {
                    $config += $this->_formatTypeConfig($value);
                }
                $class = $config["class"];
                $function = $config["method"];
                $argument = [];
                $function = new \ReflectionMethod($class, $function);
                foreach ($function->getParameters() as $i => $parameter) {
                    if (isset($config["argument"][$parameter->getName()])) {
                        $argument[$i] = $config["argument"][$parameter->getName()];
                    }
                }
                $function = $config["method"];
                $config = $class->$function(...$argument);
                break;
            default:
                $config = (string)trim($data);
                break;
        }
        return $config;
    }
}
