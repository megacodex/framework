<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\App\Config;

class AppConfig extends \MegaCodex\Framework\Data
{
    public function __construct(
        \MegaCodex\Framework\App\Config\DefaultPool $defaultPool
    ) {
        parent::__construct([]);
        $data = $defaultPool->getAppConfig();
        $data["state"] = $data["APP_MODE"];
        unset($data["APP_MODE"]);
        $data["developer_mode"] = $data["state"] == \MegaCodex\Framework\AppInterface::MODE_DEVELOPER;
        $this->setData($data);
    }
}
