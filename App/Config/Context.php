<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\App\Config;

class Context
{
    /**
     * @var \MegaCodex\Framework\App\Request
     */
    protected $_request;

    /**
     * @var \MegaCodex\Framework\App\ObjectManager
     */
    protected $_objectManager;

    /**
     * @var \MegaCodex\Framework\App\Filesystem
     */
    protected $_fileSystem;

    /**
     * @param \MegaCodex\Framework\App\ObjectManager $objectManager
     * @param \MegaCodex\Framework\App\Request $request
     * @param \MegaCodex\Framework\App\Response $response
     * @param \MegaCodex\Framework\App\Filesystem $fileSystem
     */
    public function __construct(
        \MegaCodex\Framework\App\ObjectManager $objectManager,
        \MegaCodex\Framework\App\Request $request,
        \MegaCodex\Framework\App\Filesystem $fileSystem
    ) {
        $this->_objectManager = $objectManager;
        $this->_request = $request;
        $this->_fileSystem = $fileSystem;
    }

    public function __call($name, $arguments = null)
    {
        if (substr($name, 0, 3) == "get") {
            $param = lcfirst(substr($name, 3));
            if (isset($this->{"_" . $param})) {
                return $this->{"_" . $param};
            }
        }
        return call_user_func($name, $arguments);
    }
}
