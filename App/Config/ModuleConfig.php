<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\App\Config;

class ModuleConfig
{
    const CONFIG_TYPE_MODULE = "module";
    const CONFIG_TYPE_DI = "di";
    const CONFIG_TYPE_CONFIG = "config";
    const CONFIG_TYPE_EVENT = "event";
    const CONFIG_TYPE_ACL = "acl";
    const DEFAUL_MODULE = "MegaCodex_Framework";

    private $_fileReader;
    private $_objectManager;
    private $_componentRegistrar;
    private $_modules;

    /**
     * @param \MegaCodex\Framework\App\ObjectManager $objectManager
     * @param \MegaCodex\Framework\App\Config\DefaultPool $configPool
     * @param \MegaCodex\Framework\Filesystem\Directory\ReadFactory $fileReader
     * @param \MegaCodex\Framework\ComponentRegistrar $componentRegistrar
     */
    public function __construct(
        \MegaCodex\Framework\App\ObjectManager $objectManager,
        \MegaCodex\Framework\App\Config\DefaultPool $configPool,
        \MegaCodex\Framework\App\Filesystem $filesystem,
        \MegaCodex\Framework\ComponentRegistrar $componentRegistrar
    ) {
        $this->_fileReader = $filesystem->getDirectoryRead(
            \MegaCodex\Framework\App\Filesystem\DirectoryList::ROOT,
            \MegaCodex\Framework\Filesystem\DriverPool::XML
        );
        $this->_objectManager = $objectManager;
        $this->_componentRegistrar = $componentRegistrar;
        $this->_init($configPool);
    }

    /*	 * TODO Implement chacing* */

    private function _init(\MegaCodex\Framework\App\Config\DefaultPool $configPool)
    {
        $modules = $configPool->getModuleConfig();

        foreach ($this->_componentRegistrar->getPaths($this->_componentRegistrar::MODULE)
                 as $name => $modulePath) {
            if (isset($modules[$name]) && $modules[$name]) {
                $this->_modules[$name] = $modulePath;
            }
        }
    }

    public function getConfig($configType, $section = false)
    {
        $config = false;
        foreach ($this->_modules as $name => $modulePath) {
            $modulePath = $modulePath . "etc/" . $configType . ".xml";
            if ($this->_fileReader->isFile($modulePath)) {
                $reader = $this->_fileReader->readFile($modulePath)->setElement($section);
                while ($data = $reader->next()) {
                    if (empty($config)) {
                        $config = [];
                    }
                    $config = array_merge_recursive($config, $data);
                }
            }
        }
        return $config;
    }

    public function hasModule(string $name)
    {
        $module = $this->getModule($name);
        return empty($module) ? false : true;
    }

    public function getModule(string $name)
    {
        return isset($this->_modules[$name]) ? $this->_modules[$name] : false;
    }

    public function getModules()
    {
        return $this->_modules;
    }
}
