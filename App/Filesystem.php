<?php
/**
 * MegaCodex filesystem facade
 *
 * @author    Zahirul Hasan
 * @copyright    Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 */

namespace MegaCodex\Framework\App;

use MegaCodex\Framework\Filesystem\DriverPool;

/**
 * @api
 */
class Filesystem
{
    /**
     * @var \MegaCodex\Framework\App\Filesystem\DirectoryList
     */
    protected $directoryList;

    /**
     * @var \MegaCodex\Framework\Filesystem\Directory\ReadFactory
     */
    protected $readFactory;

    /**
     * @var \MegaCodex\Framework\Filesystem\Directory\WriteFactory
     */
    protected $writeFactory;

    /**
     * @var \MegaCodex\Framework\Filesystem\Directory\ReadInterface[]
     */
    protected $readInstances = [];

    /**
     * @var \MegaCodex\Framework\Filesystem\Directory\WriteInterface[]
     */
    protected $writeInstances = [];

    /**
     * @param Filesystem\DirectoryList $directoryList
     * @param Filesystem\Directory\ReadFactory $readFactory
     * @param Filesystem\Directory\WriteFactory $writeFactory
     */
    public function __construct(
        \MegaCodex\Framework\App\Filesystem\DirectoryList $directoryList,
        \MegaCodex\Framework\Filesystem\Directory\ReadFactory $readFactory,
        \MegaCodex\Framework\Filesystem\Directory\WriteFactory $writeFactory
    ) {
        $this->directoryList = $directoryList;
        $this->readFactory = $readFactory;
        $this->writeFactory = $writeFactory;
    }

    /**
     * Create an instance of directory with read permissions
     *
     * @param string $directoryCode
     * @param string $driverCode
     *
     * @return \MegaCodex\Framework\Filesystem\Directory\ReadInterface
     */
    public function getDirectoryRead($directoryCode, $driverCode = DriverPool::FILE)
    {
        $code = $directoryCode . '_' . $driverCode;
        if (!array_key_exists($code, $this->readInstances)) {
            $this->readInstances[$code] = $this->readFactory->create($this->getDirPath($directoryCode), $driverCode);
        }
        return $this->readInstances[$code];
    }

    /**
     * Create an instance of directory with write permissions
     *
     * @param string $directoryCode
     * @param string $driverCode
     *
     * @return \MegaCodex\Framework\Filesystem\Directory\WriteInterface
     * @throws \MegaCodex\Framework\Exception\FileSystemException
     */
    public function getDirectoryWrite($directoryCode, $driverCode = DriverPool::FILE)
    {
        $code = $directoryCode . '_' . $driverCode;
        if (!array_key_exists($code, $this->writeInstances)) {
            $this->writeInstances[$code] = $this->writeFactory->create($this->getDirPath($directoryCode), $driverCode);
        }
        return $this->writeInstances[$code];
    }

    /**
     * Gets configuration of a directory
     *
     * @param string $code
     *
     * @return string
     */
    public function getDirPath($code)
    {
        return $this->directoryList->getPath($code);
    }

    /**
     * Retrieve uri for given code
     *
     * @param string $code
     *
     * @return string
     */
    public function getUri($code)
    {
        return $this->directoryList->getUrlPath($code);
    }
}
