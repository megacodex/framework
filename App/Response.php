<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\App;

class Response extends \Zend\Http\PhpEnvironment\Response implements \MegaCodex\Framework\App\ResponseInterface
{
    /** Format for expiration timestamp headers */
    const EXPIRATION_TIMESTAMP_FORMAT = "D, d M Y H:i:s T";

    /**
     * Flag; is this response a redirect?
     *
     * @var boolean
     */
    protected $_isRedirect = false;

    /**
     * @var \MegaCodex\Framework\Stdlib\DateTime
     */
    protected $_dateTime;

    protected $_filePath;

    protected $_mime;

    public function __construct(
        \MegaCodex\Framework\Stdlib\DateTime $dateTime,
        \MegaCodex\Framework\Filesystem\Mime $mime
    ) {
        $this->_dateTime = $dateTime;
        $this->_mime = $mime;
        $this->_filePath = null;
    }

    /**
     * Get header value by name.
     * Returns first found header by passed name.
     * If header with specified name was not found returns false.
     *
     * @param string $name
     *
     * @return \Zend\Http\Header\HeaderInterface|bool
     */
    public function getHeader($name)
    {
        $header = false;
        $headers = $this->getHeaders();
        if ($headers->has($name)) {
            $header = $headers->get($name);
        }
        return $header;
    }

    /**
     * Send the response, including all headers, rendering exceptions if so
     * requested.
     *
     * @return void
     */
    public function sendResponse()
    {
        if (!empty($this->_filePath) && $this->getHttpResponseCode() == 200) {
            $this->sendFile();
        } else {
            $this->send();
        }
    }

    private function sendFile()
    {
        if (empty($this->_filePath)) {
            throw new \InvalidArgumentException("Filename is not set.");
        }

        if (!is_file($this->_filePath) || !is_readable($this->_filePath)) {
            throw new \InvalidArgumentException("File '{$this->_filePath}' does not exists.");
        }

        $mimeType = $this->_mime->getMimeType($this->_filePath);

        $this->setHeader("Content-length", filesize($this->_filePath));
        $this->setHeader("Content-Type", $mimeType);

        $this->sendHeaders();

        $handle = fopen($this->_filePath, "r");
        if ($handle) {
            while (($buffer = fgets($handle, 4096)) !== false) {
                echo $buffer;
            }
            if (!feof($handle)) {
                throw new \UnexpectedValueException("Unexpected end of file");
            }
            fclose($handle);
        }
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function appendBody($value)
    {
        $body = $this->getContent();
        $this->setContent($body . $value);
        return $this;
    }

    public function setFilePath($path)
    {
        $this->_filePath = $path;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setBody($value)
    {
        $this->setContent($value);
        return $this;
    }

    /**
     * Clear body
     *
     * @return $this
     */
    public function clearBody()
    {
        $this->setContent("");
        return $this;
    }

    /**
     * Set a header
     * If $replace is true, replaces any headers already defined with that
     * $name.
     *
     * @param string $name
     * @param string $value
     * @param boolean $replace
     *
     * @return $this
     */
    public function setHeader($name, $value, $replace = false)
    {
        $value = (string)$value;

        if ($replace) {
            $this->clearHeader($name);
        }

        $this->getHeaders()->addHeaderLine($name, $value);
        return $this;
    }

    /**
     * Remove header by name from header stack
     *
     * @param string $name
     *
     * @return $this
     */
    public function clearHeader($name)
    {
        $headers = $this->getHeaders();
        if ($headers->has($name)) {
            $header = $headers->get($name);
            $headers->removeHeader($header);
        }

        return $this;
    }

    /**
     * Remove all headers
     *
     * @return $this
     */
    public function clearHeaders()
    {
        $headers = $this->getHeaders();
        $headers->clearHeaders();

        return $this;
    }

    /**
     * Set redirect URL
     * Sets Location header and response code. Forces replacement of any prior
     * redirects.
     *
     * @param string $url
     * @param int $code
     *
     * @return $this
     */
    public function setRedirect($url, $code = 302)
    {
        $this->setHeader("Location", $url, true)
             ->setHttpResponseCode($code);

        return $this;
    }

    /**
     * Set HTTP response code to use with headers
     *
     * @param int $code
     *
     * @return $this
     */
    public function setHttpResponseCode($code)
    {
        if (!is_numeric($code) || (100 > $code) || (599 < $code)) {
            throw new \InvalidArgumentException("Invalid HTTP response code");
        }

        $this->_isRedirect = (300 <= $code && 307 >= $code) ? true : false;

        $this->setStatusCode($code);
        return $this;
    }

    /**
     * @param int|string $httpCode
     * @param null|int|string $version
     * @param null|string $phrase
     *
     * @return $this
     */
    public function setStatusHeader($httpCode, $version = null, $phrase = null)
    {
        $version = $version === null ? $this->detectVersion() : $version;
        $phrase = $phrase === null ? $this->getReasonPhrase() : $phrase;

        $this->setVersion($version);
        $this->setHttpResponseCode($httpCode);
        $this->setReasonPhrase($phrase);

        return $this;
    }

    /**
     * Get response code
     *
     * @return int
     */
    public function getHttpResponseCode()
    {
        return $this->getStatusCode();
    }

    /**
     * Is this a redirect?
     *
     * @return boolean
     */
    public function isRedirect()
    {
        return $this->_isRedirect;
    }

    /**
     * @return string[]
     */
    public function __sleep()
    {
        return ["content", "isRedirect", "statusCode"];
    }

    /**
     * Set headers for public cache
     * Accepts the time-to-live (max-age) parameter
     *
     * @param int $ttl
     *
     * @return void
     * @throws \InvalidArgumentException
     */
    public function setPublicHeaders($ttl)
    {
        if ($ttl < 0 || !preg_match("/^[0-9]+$/", $ttl)) {
            throw new \InvalidArgumentException("Time to live is a mandatory parameter for set public headers");
        }
        $this->setHeader("pragma", "cache", true);
        $this->setHeader("cache-control", "public, max-age=" . $ttl . ", s-maxage=" . $ttl, true);
        $this->setHeader("expires", $this->getExpirationHeader("+" . $ttl . " seconds"), true);
    }

    /**
     * Set headers for private cache
     *
     * @param int $ttl
     *
     * @return void
     * @throws \InvalidArgumentException
     */
    public function setPrivateHeaders($ttl)
    {
        if (!$ttl) {
            throw new \InvalidArgumentException("Time to live is a mandatory parameter for set private headers");
        }
        $this->setHeader("pragma", "cache", true);
        $this->setHeader("cache-control", "private, max-age=" . $ttl, true);
        $this->setHeader("expires", $this->getExpirationHeader("+" . $ttl . " seconds"), true);
    }

    /**
     * Set headers for no-cache responses
     *
     * @return void
     * @codeCoverageIgnore
     */
    public function setNoCacheHeaders()
    {
        $this->setHeader("pragma", "no-cache", true);
        $this->setHeader("cache-control", "no-store, no-cache, must-revalidate, max-age=0", true);
        $this->setHeader("expires", $this->getExpirationHeader("-1 year"), true);
    }

    /**
     * Represents an HTTP response body in JSON format by sending appropriate header
     *
     * @param string $content String in JSON format
     *
     * @return \MegaCodex\Framework\App\Response\Http
     * @codeCoverageIgnore
     */
    public function representJson($content)
    {
        $this->setHeader("Content-Type", "application/json", true);
        return $this->setContent($content);
    }

    /**
     * Given a time input, returns the formatted header
     *
     * @param string $time
     *
     * @return string
     * @codeCoverageIgnore
     */
    protected function getExpirationHeader($time)
    {
        return $this->_dateTime->gmDate(self::EXPIRATION_TIMESTAMP_FORMAT, $this->_dateTime->strToTime($time));
    }
}
