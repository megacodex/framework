<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\App;

class Router implements RouterInterface
{

    /**
     * @var \MegaCodex\Framework\App\ActionFactory
     */
    protected $_actionFactory;

    /**
     * List of required request parameters
     * Order sensitive
     *
     * @var string[]
     */
    protected $_requiredParams = ["moduleName", "actionPath", "actionName"];

    /**
     * @var string[]
     */
    protected $_defaultPath = [
        "module"     => "megacodex",
        "controller" => "index",
        "action"     => "index",
    ];
    protected $reservedWords = [
        'abstract',
        'and',
        'array',
        'as',
        'break',
        'callable',
        'case',
        'catch',
        'class',
        'clone',
        'const',
        'continue',
        'declare',
        'default',
        'die',
        'do',
        'echo',
        'else',
        'elseif',
        'empty',
        'enddeclare',
        'endfor',
        'endforeach',
        'endif',
        'endswitch',
        'endwhile',
        'eval',
        'exit',
        'extends',
        'final',
        'for',
        'foreach',
        'function',
        'global',
        'goto',
        'if',
        'implements',
        'include',
        'instanceof',
        'insteadof',
        'interface',
        'isset',
        'list',
        'namespace',
        'new',
        'or',
        'print',
        'private',
        'protected',
        'public',
        'require',
        'return',
        'static',
        'switch',
        'throw',
        'trait',
        'try',
        'unset',
        'use',
        'var',
        'while',
        'xor',
        'void',
    ];

    protected $_unMatchedRoutes;

    /**
     * @var \MegaCodex\Framework\App\Request
     */
    protected $_request;

    protected $_params;

    /**
     * @param \MegaCodex\Framework\App\Request $request
     * @param \MegaCodex\Framework\App\ActionFactory $actionFactory
     * @param \MegaCodex\Framework\App\Config\ModuleConfig $moduleConfig
     */
    public function __construct(
        \MegaCodex\Framework\App\Request $request,
        \MegaCodex\Framework\App\ActionFactory $actionFactory,
        \MegaCodex\Framework\App\Config\ModuleConfig $moduleConfig
    ) {
        $this->_request = $request;
        $this->_actionFactory = $actionFactory;
        $this->_moduleConfig = $moduleConfig;
        $this->_unMatchedRoutes = [];
        $this->_params = $this->parseRequest($this->_request);
    }

    /**
     * Modify request and set to no-route action
     *
     * @return ActionInterface
     */
    public function match()
    {
        $moduleName = $this->matchModuleName($this->_params["moduleName"]);

        /**
         * Searching router args by module name from route using it as key
         */
        $modules = $this->getModulesByRoute($moduleName);

        /**
         * Going through modules to find appropriate controller
         */
        $currentModuleName = null;
        $actionPath = null;
        $action = null;
        $actionInstance = null;

        $actionPath = $this->matchActionPath($this->_params["actionPath"]);
        $action = $this->_request->getActionName() ?: ($this->_params["actionName"] ?: $this->_defaultPath["action"]);

        foreach ($modules as $moduleName) {
            if (in_array($moduleName . "_" . $actionPath . "_" . $action, $this->_unMatchedRoutes)) {
                continue;
            }

            $currentModuleName = $moduleName;

            $actionClassName = $this->getActionClassName($moduleName, $actionPath, $action);
            if (!$actionClassName || !is_subclass_of($actionClassName, "\MegaCodex\Framework\App\Action\AbstractAction")) {
                $this->_unMatchedRoutes[] = $moduleName . "_" . $actionPath . "_" . $action;
                continue;
            }

            $actionInstance = $this->_actionFactory->create($actionClassName);
            break;
        }

        if (empty($currentModuleName)) {
            $this->_request->setModuleName($this->_defaultPath["module"]);
        }

        if (empty($actionInstance)) {
            $this->_request->setControllerName("noroute");
            return null;
        }

        // set values only after all the checks are done
        $this->_request->setModuleName($moduleName);
        $this->_request->setControllerName($actionPath);
        $this->_request->setActionName($action);
        $this->_request->setControllerModule($currentModuleName);
        if (isset($params["variables"])) {
            $this->_request->setParams($params["variables"]);
        }
        return $actionInstance;
    }

    /**
     * Parse request URL params
     *
     * @return array
     */
    protected function parseRequest()
    {
        $output = [];

        $path = trim($this->_request->getPathInfo(), "/");

        $params = explode("/", $path ? $path : "/");
        foreach ($this->_requiredParams as $paramName) {
            $output[$paramName] = array_shift($params);
        }

        for ($i = 0, $l = sizeof($params); $i < $l; $i += 2) {
            $output["variables"][$params[$i]] = isset($params[$i + 1]) ? urldecode($params[$i + 1]) : "";
        }
        return $output;
    }

    /**
     * Match module front name
     *
     * @param string $param
     *
     * @return string|null
     */
    protected function matchModuleName($param)
    {
        // get module name
        if ($this->_request->getModuleName()) {
            $moduleName = $this->_request->getModuleName();
        } elseif (!empty($param)) {
            $moduleName = $param;
        } else {
            $moduleName = $this->_defaultPath["module"];
        }
        if (!$moduleName) {
            return null;
        }
        return $moduleName;
    }

    /**
     * Match controller name
     *
     * @param string $param
     *
     * @return string
     */
    protected function matchActionPath($param)
    {
        if ($this->_request->getControllerName()) {
            $actionPath = $this->_request->getControllerName();
        } elseif (!empty($param)) {
            $actionPath = $param;
        } else {
            $actionPath = $this->_defaultPath["controller"];
        }
        return $actionPath;
    }

    public function getActionClassName($moduleName, $actionPath, $action)
    {
        if (in_array(strtolower($action), $this->reservedWords)) {
            $action .= "action";
        }
        $path = "\\" . ucfirst($moduleName) . "\\Controller\\" . ucfirst($actionPath) . '\\' . ucfirst($action);
        $path = explode("_", $path);
        $path = join("\\", array_map("ucfirst", $path));

        return is_subclass_of($path, "\MegaCodex\Framework\App\Action\AbstractAction") ? $path : null;
    }

    /**
     * Check whether redirect url should be used for secure routes
     *
     * @return bool
     */
    protected function _shouldRedirectToSecure()
    {
        return $this->_url->getUseSession();
    }

    protected function getModulesByRoute($routeName)
    {
        $matchedModules = [];
        $routes = $this->_moduleConfig
            ->getConfig(Config\ModuleConfig::CONFIG_TYPE_MODULE, "route");

        if (!empty($routes)) {
            $routes = $routes["route"];
            foreach ($routes as $route) {
                if ($route->getName() == $routeName) {
                    if (empty($matchedModules)) {
                        $matchedModules = [];
                    }
                    $modules = $route->getModule();
                    foreach ($modules as $module) {
                        $matchedModules[] = $module->getName();
                    }
                }
            }
        }

        return $matchedModules;
    }
}
