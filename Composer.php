<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework;

class Composer
{
    const _ignoredFiles = ["composer.json", ".htaccess", "app/etc/config.php", "app/etc/modules.php"];

    /**
     * @param \Composer\Script\Event $event
     */
    public static function deployRootFiles(\Composer\Script\Event $event)
    {
        $io = $event->getIO();
        $rootDir = dirname($event->getComposer()->getConfig()->get("vendor-dir")) . DIRECTORY_SEPARATOR;
        $ignoredFiles = [];
        foreach (self::_ignoredFiles as &$file) {
            $ignoredFiles[] = $rootDir . $file;
        }
        $pool = new \MegaCodex\Framework\Filesystem\DriverPool();
        $fs = $pool->getDriver(\MegaCodex\Framework\Filesystem\DriverPool::FILE);

        $rootFilesPath = dirname(__FILE__) . DIRECTORY_SEPARATOR . "RootFiles" . DIRECTORY_SEPARATOR;
        $files = $fs->readDirectoryRecursively($rootFilesPath);
        asort($files);
        foreach ($files as $file) {
            $dest = str_replace($rootFilesPath, $rootDir, $file);

            if ($fs->isExists($dest)) {
                if (in_array($dest, $ignoredFiles)) {
                    $io->write(sprintf("<info>Destination <comment>%s</comment> already exist skipping</info>", $dest));
                    continue;
                }
            }

            if (!$fs->isExists($file)) {
                throw new \InvalidArgumentException(
                    sprintf("<error>Source directory or file <comment>%s</comment> does not exist.</error>", $from)
                );
            }

            if ($fs->isFile($file)) {
                $fs->copy($file, $dest);
                $io->write(sprintf("Copied file from <comment>%s</comment> to <comment>%s</comment>.", $file, $dest));
            }

            if ($fs->isDirectory($file)) {
                $fs->createDirectory($dest);
                $io->write(sprintf("Directory <comment>%s</comment> created.", $dest));
            }
        }
    }
}
