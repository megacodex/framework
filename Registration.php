<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

\MegaCodex\Framework\ComponentRegistrar::register(
    \MegaCodex\Framework\ComponentRegistrar::MODULE,
    "MegaCodex_Framework",
    __DIR__
);
