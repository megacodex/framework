<?php

/**
 * Origin filesystem driver
 *
 * @copyright    Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 */

namespace MegaCodex\Framework\Filesystem\Driver;

/**
 * Class File
 *
 * @package MegaCodex\Framework\Filesystem\Driver
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class XML extends File
{

    /**
     * @var \XMLReader
     */
    protected $xml;
    protected $elementName;

    public function fileGetContents($path, $flag = null, $context = null)
    {
        clearstatcache();
        $this->xml = new \XMLReader();
        $result = $this->xml->open($path, LIBXML_PARSEHUGE);
        if (false === $result) {
            throw new FileSystemException(new \MegaCodex\Framework\Phrase(
                                              "Cannot read contents from file \"%1\" %2",
                                              [$path, $this->getWarningMessage()]
                                          ));
        }
        while ($this->xml->read()) {
            if ($this->xml->nodeType == \XMLReader::ELEMENT) {
                $this->xml->read();
                break;
            }
        }
        return $this;
    }

    public function setElement($elementName)
    {
        $this->elementName = $elementName;
        return $this;
    }

    public function next()
    {
        while ($this->xml->next()) {
            if ($this->xml->nodeType == \XMLReader::ELEMENT) {
                if (!empty($this->elementName)) {
                    if ($this->xml->name == $this->elementName) {
                        break;
                    }
                } else {
                    break;
                }
            }
        }
        $xmlData = trim($this->xml->readOuterXML());
        if (!empty($xmlData)) {
            return $this->parseXMLString($xmlData);
        }
        return false;
    }

    private function parseXML($xml)
    {
        $data = false;
        while ($xml->read()) {
            switch ($xml->nodeType) {
                case \XMLReader::END_ELEMENT:
                    return $data;
                case \XMLReader::ELEMENT:
                    $node = new \MegaCodex\Framework\Data();
                    $value = $xml->isEmptyElement ? "" : $this->parseXML($xml);
                    if (is_array($value)) {
                        foreach ($value as $key => $val) {
                            //var_dump($val);
                            $node->{"set" . ucfirst($key)}($val);
                        }
                    }

                    $node->setValue($value);
                    $node->setTag($xml->name);

                    if ($xml->hasAttributes) {
                        while ($xml->moveToNextAttribute()) {
                            $node->{"set" . ucfirst($xml->name)}($xml->value);
                        }
                    }
                    if (empty($data)) {
                        $data = [];
                    }
                    if (empty($data[$node->getTag()])) {
                        $data[$node->getTag()] = [];
                    }
                    $data[$node->getTag()][] = $node;
                    break;
                case \XMLReader::TEXT:
                case \XMLReader::CDATA:
                    $data .= $xml->value;
            }
        }

        return $data;
    }

    protected function parseXMLString(string $xmlString)
    {
        $xml = new \XMLReader();
        $xml->XML($xmlString);
        $data = $this->parseXML($xml);
        return $data;
    }
}
