<?php
/**
 * @copyright    Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 */

namespace MegaCodex\Framework\Filesystem\File;

use MegaCodex\Framework\Filesystem\DriverInterface;
use MegaCodex\Framework\Filesystem\DriverPool;

class WriteFactory
{
    /**
     * Pool of filesystem drivers
     *
     * @var DriverPool
     */
    private $driverPool;

    /**
     * Constructor
     *
     * @param DriverPool $driverPool
     */
    public function __construct(DriverPool $driverPool)
    {
        $this->driverPool = $driverPool;
    }

    /**
     * Create a readable file.
     *
     * @param string $path
     * @param DriverInterface|string $driver Driver or driver code
     * @param string $mode [optional]
     *
     * @return Write
     */
    public function create($path, $driver, $mode = 'r')
    {
        if (is_string($driver)) {
            return new Write($path, $this->driverPool->getDriver($driver), $mode);
        }
        return new Write($path, $driver, $mode);
    }
}
