<?php
/**
 * @copyright    Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 */

namespace MegaCodex\Framework\Filesystem\File;

interface WriteInterface extends ReadInterface
{
    /**
     * Writes the data to file.
     *
     * @param string $data
     *
     * @return int
     * @throws \MegaCodex\Framework\Exception\FileSystemException
     */
    public function write($data);

    /**
     * Writes one CSV row to the file.
     *
     * @param array $data
     * @param string $delimiter
     * @param string $enclosure
     *
     * @return int
     * @throws \MegaCodex\Framework\Exception\FileSystemException
     */
    public function writeCsv(array $data, $delimiter = ',', $enclosure = '"');

    /**
     * Flushes the output.
     *
     * @return bool
     * @throws \MegaCodex\Framework\Exception\FileSystemException
     */
    public function flush();

    /**
     * Portable advisory file locking
     *
     * @param int $lockMode
     *
     * @return bool
     */
    public function lock($lockMode = LOCK_EX);

    /**
     * File unlocking
     *
     * @return bool
     */
    public function unlock();
}
