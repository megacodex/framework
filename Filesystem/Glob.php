<?php
/**
 * @copyright    Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 */

namespace MegaCodex\Framework\Filesystem;

use Zend\Stdlib\Exception\RuntimeException as ZendRuntimeException;
use Zend\Stdlib\Glob as ZendGlob;

/**
 * Wrapper for Zend\Stdlib\Glob
 */
class Glob extends ZendGlob
{
    /**
     * Find pathnames matching a pattern.
     *
     * @param string $pattern
     * @param int $flags
     * @param bool $forceFallback
     *
     * @return array
     */
    public static function glob($pattern, $flags = 0, $forceFallback = false)
    {
        try {
            $result = ZendGlob::glob($pattern, $flags, $forceFallback);
        } catch (ZendRuntimeException $e) {
            $result = [];
        }
        return $result;
    }
}
