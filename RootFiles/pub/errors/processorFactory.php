<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\Error;

require_once realpath(__DIR__) . "/../../app/bootstrap.php";
require_once "processor.php";

/**
 * Error processor factory
 */
class ProcessorFactory
{
    /**
     * Create Processor
     *
     * @return Processor
     */
    public function createProcessor()
    {
        $bootstrap = \MegaCodex\Framework\App\Bootstrap::create(BP, $_SERVER);
        $objectManager = $bootstrap->getObjectManager();
        $response = $objectManager->create(\MegaCodex\Framework\App\Response::class);
        return new Processor($response);
    }
}
