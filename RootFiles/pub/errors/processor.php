<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\Error;

/**
 * Error processor
 * @SuppressWarnings(PHPMD.TooManyFields)
 */
class Processor
{
    const ERROR_DIR = "pub/errors";

    /**
     * Page title
     *
     * @var string
     */
    public $pageTitle;

    /**
     * Base URL
     *
     * @var string
     */
    public $baseUrl;

    /**
     * Post data
     *
     * @var array
     */
    public $postData;

    /**
     * Report data
     *
     * @var array
     */
    public $reportData;

    /**
     * Report ID
     *
     * @var int
     */
    public $reportId;

    /**
     * Report file
     *
     * @var string
     */
    protected $_reportFile;

    /**
     * Show error message
     *
     * @var bool
     */
    public $showErrorMsg;

    /**
     * Show message after sending email
     *
     * @var bool
     */
    public $showSentMsg;

    /**
     * Show form for sending
     *
     * @var bool
     */
    public $showSendForm;

    /**
     * @var string
     */
    public $reportUrl;

    /**
     * Server script name
     *
     * @var string
     */
    protected $_scriptName;

    /**
     * Is root
     *
     * @var bool
     */
    protected $_root;

    /**
     * Internal config object
     *
     * @var \stdClass
     */
    protected $_config;

    /**
     * Http response
     *
     * @var \MegaCodex\Framework\App\Response\Http
     */
    protected $_response;

    /**
     * @param \MegaCodex\Framework\App\Response\Http $response
     */
    public function __construct(\MegaCodex\Framework\App\Response $response)
    {
        $this->_response = $response;
        $this->_errorDir = __DIR__ . "/";
        $this->_reportDir = dirname(dirname($this->_errorDir)) . "/var/report/";

        if (!empty($_SERVER["SCRIPT_NAME"])) {
            if (in_array(basename($_SERVER["SCRIPT_NAME"], ".php"), ["404", "503", "report"])) {
                $this->_scriptName = dirname($_SERVER["SCRIPT_NAME"]);
            } else {
                $this->_scriptName = $_SERVER["SCRIPT_NAME"];
            }
        }

        $reportId = (isset($_GET["id"])) ? (int)$_GET["id"] : null;
        if ($reportId) {
            $this->loadReport($reportId);
        }

        $this->_indexDir = $this->_getIndexDir();
        $this->_root = is_dir($this->_indexDir . "app");

        $this->_prepareConfig();
    }

    /**
     * Process no cache error
     *
     * @return \MegaCodex\Framework\App\Response\Http
     */
    public function processNoCache()
    {
        $this->pageTitle = "Error : cached config data is unavailable";
        $this->_response->setBody($this->_renderPage("nocache.phtml"));
        return $this->_response;
    }

    /**
     * Process 404 error
     *
     * @return \MegaCodex\Framework\App\Response\Http
     */
    public function process404()
    {
        $this->pageTitle = "Error 404: Not Found";
        $this->_response->setHttpResponseCode(404);
        $this->_response->setBody($this->_renderPage("404.phtml"));
        return $this->_response;
    }

    /**
     * Process 503 error
     *
     * @return \MegaCodex\Framework\App\Response\Http
     */
    public function process503()
    {
        $this->pageTitle = "Error 503: Service Unavailable";
        $this->_response->setHttpResponseCode(503);
        $this->_response->setBody($this->_renderPage("503.phtml"));
        return $this->_response;
    }

    /**
     * Process report
     *
     * @return \MegaCodex\Framework\App\Response\Http
     */
    public function processReport()
    {
        $this->pageTitle = "There has been an error processing your request";
        $this->_response->setHttpResponseCode(503);

        $this->showErrorMsg = false;
        $this->showSentMsg = false;
        $this->showSendForm = false;
        $this->_setReportUrl();

        $this->_response->setBody($this->_renderPage("report.phtml"));
        return $this->_response;
    }

    /**
     * @return string
     */
    public function getViewFileUrl()
    {
        //The url needs to be updated base on Document root path.
        return $this->getBaseUrl() .
               str_replace(
                   str_replace("\\", "/", $this->_indexDir),
                   "",
                   str_replace("\\", "/", $this->_errorDir)
               );
    }

    /**
     * Retrieve base host URL without path
     *
     * @return string
     */
    public function getHostUrl()
    {
        /**
         * Define server http host
         */
        if (!empty($_SERVER["HTTP_HOST"])) {
            $host = $_SERVER["HTTP_HOST"];
        } elseif (!empty($_SERVER["SERVER_NAME"])) {
            $host = $_SERVER["SERVER_NAME"];
        } else {
            $host = "localhost";
        }

        $isSecure = (!empty($_SERVER["HTTPS"])) && ($_SERVER["HTTPS"] != "off");
        $url = ($isSecure ? "https://" : "http://") . $host;

        if (!empty($_SERVER["SERVER_PORT"]) && !in_array($_SERVER["SERVER_PORT"], [80, 433])
            && !preg_match("/.*?\:[0-9]+$/", $url)
        ) {
            $url .= ":" . $_SERVER["SERVER_PORT"];
        }
        return $url;
    }

    /**
     * Retrieve base URL
     *
     * @param bool $param
     *
     * @return string
     */
    public function getBaseUrl($param = false)
    {
        $path = $this->_scriptName;

        if ($param && !$this->_root) {
            $path = dirname($path);
        }

        $basePath = str_replace("\\", "/", dirname($path));
        return $this->getHostUrl() . ("/" == $basePath ? "" : $basePath) . "/";
    }

    /**
     * Retrieve client IP address
     *
     * @return string
     */
    protected function _getClientIp()
    {
        return (isset($_SERVER["REMOTE_ADDR"])) ? $_SERVER["REMOTE_ADDR"] : "undefined";
    }

    /**
     * Get index dir
     *
     * @return string
     */
    protected function _getIndexDir()
    {
        $documentRoot = "";
        if (!empty($_SERVER["DOCUMENT_ROOT"])) {
            $documentRoot = rtrim(realpath($_SERVER["DOCUMENT_ROOT"]), "/");
        }
        return dirname($documentRoot . $this->_scriptName) . "/";
    }

    /**
     * Prepare config data
     *
     * @return void
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function _prepareConfig()
    {
        //initial settings
        $config = new \stdClass();
        $config->subject = "Store Debug Information";
        $config->trash = "leave";

        $this->_config = $config;
    }

    /**
     * @param string $template
     *
     * @return string
     */
    protected function _renderPage($template)
    {
        $baseTemplate = $this->_getFilePath("page.phtml");
        $contentTemplate = $this->_getFilePath($template);

        $html = "";
        if ($baseTemplate && $contentTemplate) {
            ob_start();
            require_once $baseTemplate;
            $html = ob_get_clean();
        }
        return $html;
    }

    /**
     * Find file path
     *
     * @param string $file
     * @param array $directories
     *
     * @return string
     */
    protected function _getFilePath($file, $directories = null)
    {
        if ($directories === null) {
            $directories[] = $this->_errorDir . DIRECTORY_SEPARATOR . "default" . DIRECTORY_SEPARATOR;
        }

        foreach ($directories as $directory) {
            if (file_exists($directory . $file)) {
                return $directory . $file;
            }
        }
    }

    /**
     * Set report data
     *
     * @param array $reportData
     *
     * @return void
     */
    protected function _setReportData($reportData)
    {
        $this->reportData = $reportData;

        if (!isset($reportData["url"])) {
            $this->reportData["url"] = "";
        } else {
            $this->reportData["url"] = $this->getHostUrl() . $reportData["url"];
        }

        if ($this->reportData["script_name"]) {
            $this->_scriptName = $this->reportData["script_name"];
        }
    }

    /**
     * Create report
     *
     * @param array $reportData
     *
     * @return string
     */
    public function saveReport($reportData)
    {
        $this->reportData = $reportData;
        $this->reportId = abs(intval(microtime(true) * rand(100, 1000)));
        $this->_reportFile = $this->_reportDir . "/" . $this->reportId;
        $this->_setReportData($reportData);

        if (!file_exists($this->_reportDir)) {
            @mkdir($this->_reportDir, 0777, true);
        }

        @file_put_contents($this->_reportFile, $reportData);

        $this->_setReportUrl();

        return $this->reportUrl;
    }

    /**
     * Get report
     *
     * @param int $reportId
     *
     * @return void
     * @SuppressWarnings(PHPMD.ExitExpression)
     */
    public function loadReport($reportId)
    {
        $this->reportId = $reportId;
        $this->_reportFile = $this->_reportDir . "/" . $reportId;

        if (!file_exists($this->_reportFile) || !is_readable($this->_reportFile)) {
            header("Location: " . $this->getBaseUrl());
            die();
        }
        $this->_setReportData(file_get_contents($this->_reportFile));
    }

    /**
     * Validate submitted post data
     *
     * @return bool
     */
    protected function _validate()
    {
        $email = preg_match(
            "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/",
            $this->postData["email"]
        );
        return ($this->postData["firstName"] && $this->postData["lastName"] && $email);
    }

    /**
     * Set current report URL from current params
     *
     * @return void
     */
    protected function _setReportUrl()
    {
        if ($this->reportId && $this->_config) {
            $this->reportUrl = "{$this->getBaseUrl(true)}pub/errors/report.php?"
                               . http_build_query(["id" => $this->reportId]);
        }
    }
}
