<?php
/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */
if (isset($_SERVER["APP_MODE"]) && isset($_SERVER["APP_MODE"]) == "developer") {
    ini_set("display_errors", 1);
    ini_set("display_startup_errors", 1);
    error_reporting(E_ALL);
}
define("BP", dirname(__DIR__));

$vendorAutoload = BP . "/vendor/autoload.php";

/* "composer install" validation */
if (file_exists($vendorAutoload)) {
    $composerAutoloader = include $vendorAutoload;
} else {
    throw new \Exception(
        "Vendor autoload is not found. Please run \"composer install\" under application root directory."
    );
}

if (empty($_SERVER["ENABLE_IIS_REWRITES"]) || ($_SERVER["ENABLE_IIS_REWRITES"] != 1)) {
    /*
     * Unset headers used by IIS URL rewrites.
     */
    unset($_SERVER["HTTP_X_REWRITE_URL"]);
    unset($_SERVER["HTTP_X_ORIGINAL_URL"]);
    unset($_SERVER["IIS_WasUrlRewritten"]);
    unset($_SERVER["UNENCODED_URL"]);
    unset($_SERVER["ORIG_PATH_INFO"]);
}

date_default_timezone_set("UTC");

/*  For data consistency between displaying (printing) and serialization a float number */
ini_set("precision", 14);
ini_set("serialize_precision", 14);
