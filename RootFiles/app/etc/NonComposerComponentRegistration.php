<?php
/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

$pathList[] = dirname(__DIR__) . '/code/*/*/Commands.php';
$pathList[] = dirname(__DIR__) . '/code/*/*/Registration.php';
$pathList[] = dirname(__DIR__) . '/design/*/*/Registration.php';
$pathList[] = dirname(__DIR__) . '/i18n/*/*/Registration.php';
foreach ($pathList as $path) {
    $files = glob($path, GLOB_NOSORT);
    if ($files === false) {
        throw new \RuntimeException('glob() returned error while searching in \'' . $path . '\'');
    }
    foreach ($files as $file) {
        include $file;
    }
}
