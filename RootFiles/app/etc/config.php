<?php

return [
    "crypt"           => "",
    "session"         => "files",
    "db"              => [
        "table_prefix" => "",
        "host"         => "localhost",
        "dbname"       => "",
        "username"     => "",
        "password"     => "",
    ],
    "x-frame-options" => "SAMEORIGIN",
    "APP_MODE"        => "production",

    //Temp
    "recompile_css"   => false,
];
