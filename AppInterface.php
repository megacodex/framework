<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework;

/**
 * @api
 */
interface AppInterface
{
    const MODE_DEVELOPER = "developer";

    const MODE_PRODUCTION = "production";

    /**
     * Default application locale
     */
    const DEFAULT_LOCALE = "en_US";

    /**
     * Launch application
     *
     * @return \MegaCodex\Framework\App\ResponseInterface
     */
    public function launch();

    /**
     * Ability to handle exceptions that may have occurred during bootstrap and launch
     * Return values:
     * - true: exception has been handled, no additional action is needed
     * - false: exception has not been handled - pass the control to Bootstrap
     *
     * @param \MegaCodex\Framework\App\Bootstrap $bootstrap
     * @param \Exception $exception
     *
     * @return bool
     */
    public function catchException(\MegaCodex\Framework\App\Bootstrap $bootstrap, \Exception $exception);
}
