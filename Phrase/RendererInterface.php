<?php
/**
 * Phrase renderer interface
 *
 * @copyright    Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 */

namespace MegaCodex\Framework\Phrase;

/**
 * Translated phrase renderer
 *
 * @api
 */
interface RendererInterface
{
    /**
     * Render source text
     *
     * @param [] $source
     * @param [] $arguments
     *
     * @return string
     */
    public function render(array $source, array $arguments);
}
