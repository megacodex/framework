<?php
/**
 * Translate Inline Phrase renderer
 *
 * @copyright    Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 */

namespace MegaCodex\Framework\Phrase\Renderer;

use MegaCodex\Framework\Phrase\RendererInterface;
use MegaCodex\Framework\Translate\Inline\ProviderInterface;
use MegaCodex\Framework\TranslateInterface;
use Psr\Log\LoggerInterface;

class Inline implements RendererInterface
{
    /**
     * @var \MegaCodex\Framework\TranslateInterface
     */
    protected $translator;

    /**
     * @var \MegaCodex\Framework\Translate\Inline\ProviderInterface
     */
    protected $inlineProvider;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @param \MegaCodex\Framework\TranslateInterface $translator
     * @param \MegaCodex\Framework\Translate\Inline\ProviderInterface $inlineProvider
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        TranslateInterface $translator,
        ProviderInterface $inlineProvider,
        LoggerInterface $logger
    ) {
        $this->translator = $translator;
        $this->inlineProvider = $inlineProvider;
        $this->logger = $logger;
    }

    /**
     * Render source text
     *
     * @param [] $source
     * @param [] $arguments
     *
     * @return string
     * @throws \Exception
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function render(array $source, array $arguments)
    {
        $text = end($source);

        try {
            if (!$this->inlineProvider->get()->isAllowed()) {
                return $text;
            }

            if (strpos($text, '{{{') === false
                || strpos($text, '}}}') === false
                || strpos($text, '}}{{') === false
            ) {
                $text = '{{{'
                        . implode('}}{{', array_reverse($source))
                        . '}}{{' . $this->translator->getTheme() . '}}}';
            }
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
            throw $e;
        }

        return $text;
    }
}
