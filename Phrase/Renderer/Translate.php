<?php
/**
 * Translate Phrase renderer
 *
 * @copyright    Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 */

namespace MegaCodex\Framework\Phrase\Renderer;

use MegaCodex\Framework\Phrase\RendererInterface;
use MegaCodex\Framework\TranslateInterface;
use Psr\Log\LoggerInterface;

class Translate implements RendererInterface
{
    /**
     * @var \MegaCodex\Framework\TranslateInterface
     */
    protected $translator;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * Renderer construct
     *
     * @param \MegaCodex\Framework\TranslateInterface $translator
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        TranslateInterface $translator,
        LoggerInterface $logger
    ) {
        $this->translator = $translator;
        $this->logger = $logger;
    }

    /**
     * Render source text
     *
     * @param [] $source
     * @param [] $arguments
     *
     * @return string
     * @throws \Exception
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function render(array $source, array $arguments)
    {
        $text = end($source);
        /* If phrase contains escaped quotes then use translation for phrase with non-escaped quote */
        $text = str_replace('\"', '"', $text);
        $text = str_replace("\\'", "'", $text);

        try {
            $data = $this->translator->getData();
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
            throw $e;
        }

        return array_key_exists($text, $data) ? $data[$text] : end($source);
    }
}
