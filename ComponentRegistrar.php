<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework;

class ComponentRegistrar
{
    /**#@+
     * Different types of components
     */
    const MODULE = "module";
    const THEME = "theme";
    const LANGUAGE = "language";
    /**#@- */

    /**#@- */
    private static $paths = [
        self::MODULE   => [],
        self::LANGUAGE => [],
        self::THEME    => [],
    ];

    /**
     * Sets the location of a component.
     *
     * @param string $type component type
     * @param string $componentName Fully-qualified component name
     * @param string $path Absolute file path to the component
     *
     * @return void
     * @throws \LogicException
     */
    public static function register($type, $componentName, $path)
    {
        self::validateType($type);
        if (isset(self::$paths[$type][$componentName])) {
            throw new \LogicException(
                ucfirst($type) . " \"" . $componentName . "\" from \"" . $path . "\" "
                . "has been already defined in \"" . self::$paths[$type][$componentName] . "\"."
            );
        } else {
            $path = trim(str_replace(BP, "", str_replace("\\", "/", $path)), "/");
            self::$paths[$type][$componentName] = $path . DIRECTORY_SEPARATOR;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getPaths($type)
    {
        self::validateType($type);
        return self::$paths[$type];
    }

    /**
     * {@inheritdoc}
     */
    public function getPath($type, $componentName)
    {
        self::validateType($type);
        return isset(self::$paths[$type][$componentName]) ? self::$paths[$type][$componentName] : null;
    }

    /**
     * Checks if type of component is valid
     *
     * @param string $type
     *
     * @return void
     * @throws \LogicException
     */
    private static function validateType($type)
    {
        if (!isset(self::$paths[$type])) {
            throw new \LogicException("\"" . $type . "\" is not a valid component type");
        }
    }
}
