<?php

/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

function __()
{
    $argc = func_get_args();

    $text = array_shift($argc);
    if (!empty($argc) && is_array($argc[0])) {
        $argc = $argc[0];
    }

    return new \MegaCodex\Framework\Phrase($text, $argc);
}
