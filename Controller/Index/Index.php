<?php
/*
 * @author	Zahirul Hasan
 * @copyright	Copyright (c) 2018 Zahirul Hasan (http://zbabu.com)
 * @license   See LICENSE.txt for license details.
 * =====================================================================
 */

namespace MegaCodex\Framework\Controller\Index;

class Index extends \MegaCodex\Framework\App\Action\AbstractAction
{
    public function __construct(
        \MegaCodex\Framework\App\Action\Context $context,
        \MegaCodex\Framework\View\Result\Page $resultResponse
    ) {
        parent::__construct($context, $resultResponse);
    }

    protected function execute()
    {
        parent::execute();
    }
}
